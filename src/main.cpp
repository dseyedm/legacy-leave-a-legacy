/**
    Leave a Legacy Prototype
    Soraj Seyed Mahmoud - Nathan Wiebe Neufeldt
    Jan 13 - 2013
    contact: sorajsm@gmail.com
**/
#include "globals/globals.h"                // include global variables and objects
#include "state/states/states.h"                   // include program states
int main() {
    // seed the random number generator
    srand(time(NULL));
    // load all sounds
    cLogManager::getInstance()->log("Initializing sounds...\n",LOG::DIR,LOG::FILE);
    cSoundManager::getInstance()->load(FILEPATH::SOUND::CLICK_3);
    cSoundManager::getInstance()->load(FILEPATH::SOUND::CLICK_4);
    cSoundManager::getInstance()->load(FILEPATH::SOUND::ERROR_1);
    // create the window
    cLogManager::getInstance()->log("Initializing window...\n",LOG::DIR,LOG::FILE);
#ifdef G_DEBUG
    Window.create(sf::VideoMode(800,600,32),"Leave A Legacy - Soraj and Nathan [DEBUG VERSION]",sf::Style::Close);
#else
    Window.create(sf::VideoMode(800,600,32),"Leave A Legacy - Soraj and Nathan",sf::Style::Close);
#endif
    // set the state
    cStateManager::getInstance()->setState(STATE::MAIN_MENU::INSTANCE());
    // enter the main loop
    while(Window.isOpen() && !cErrorManager::getInstance()->error()) {
        // update current state
        cStateManager::getInstance()->updateState();
        // render current state
        cStateManager::getInstance()->renderState();
    }
    cLogManager::getInstance()->log("Exiting...\n",LOG::DIR,LOG::FILE);

    // delete all singletons
    cStateManager::deleteInstance();
    cLogManager  ::deleteInstance();
    cErrorManager::deleteInstance();
    cGUIManager  ::deleteInstance();
    cSoundManager::deleteInstance();

    // return application successful
    return 0;
}
