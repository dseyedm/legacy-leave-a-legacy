#include "cStateManager.h"
#include "../states/states.h"
cStateManager::~cStateManager() {
    if(getState() != NULL)getState()->destroy();  // delete the current state
}

cStateManager *cStateManager::instance = NULL;
cStateManager *cStateManager::getInstance() {
    if(instance == NULL) {
        instance = new cStateManager;
        instance->setState(STATE::DEFAULT::INSTANCE());
    }
    return instance;
}
void cStateManager::setState(cState *state) {
    if(state != NULL) {
        if(CurrentState != NULL) {
            CurrentState->destroy();  // delete the last state
        }
        CurrentState = state;       // set the current state
        CurrentState->create();     // start the new state
        cLogManager::getInstance()->log("Set state to " + CurrentState->STATE_NAME + "\n",LOG::DIR,LOG::FILE);   // log the state change
    } else {
        cLogManager::getInstance()->log(ERR::LOGM::SET_STATE_NULL,LOG::DIR,LOG::FILE);
        if(CurrentState != NULL) {
            CurrentState->destroy();  // delete the last state
        }
        cErrorManager::getInstance()->displayError(ERR::LOGM::SET_STATE_NULL,LOG::DIR,LOG::FILE);
        Window.close();
    }
}
cState *cStateManager::getState() {
    return CurrentState;
}
void cStateManager::updateState() {
    if(Window.isOpen()) {
        static sf::Event Event;
        while(Window.pollEvent(Event)) {
            if(Event.type == sf::Event::Closed) {
                Window.close();
            } else if(Event.type == sf::Event::KeyPressed) {
                if(Event.key.code == sf::Keyboard::Escape) {
                    Window.close();
                }
            }
        }
        // update the current state
        if(getState() != NULL) {
            getState()->update();
        } else {
            cLogManager::getInstance()->log(ERR::LOGM::GET_STATE_NULL,LOG::DIR,LOG::FILE);
            cErrorManager::getInstance()->displayError(ERR::LOGM::GET_STATE_NULL,LOG::DIR,LOG::FILE);
        }
    } else {
        // delete the current state
        if(getState() != NULL) {
            getState()->destroy();
        } else {
            cLogManager::getInstance()->log(ERR::LOGM::GET_STATE_NULL,LOG::DIR,LOG::FILE);
            cErrorManager::getInstance()->displayError(ERR::LOGM::GET_STATE_NULL,LOG::DIR,LOG::FILE);
        }
    }
}
void cStateManager::renderState() {
    if(getState() != NULL) {
        getState()->render();
    } else {
        cLogManager::getInstance()->log(ERR::LOGM::GET_STATE_NULL,LOG::DIR,LOG::FILE);
        cErrorManager::getInstance()->displayError(ERR::LOGM::GET_STATE_NULL,LOG::DIR,LOG::FILE);
    }
}
