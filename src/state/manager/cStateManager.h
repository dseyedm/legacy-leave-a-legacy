/**
    cStateManger is a manager class, which calls
    the update and render functions for the current active state.
**/
#ifndef CSTATEMANAGER_H
#define CSTATEMANAGER_H
#include "../../globals/globals.h"
#include "../cState/cState.h"
class cState;
class cStateManager {
private:
    static cStateManager *instance;
    cStateManager() {
        CurrentState = NULL;
    };
    ~cStateManager();

    cState *CurrentState;
public:
    static cStateManager *getInstance();
    static void deleteInstance() {
        delete instance;
        instance = NULL;
    }

    void setState(cState *);
    cState *getState();
    void updateState();
    void renderState();
};
#endif // CSTATEMANAGER_H
