/**
    cState is a virtual class, which every program state inherits from.
    The state manager calls these functions.
**/
#ifndef CSTATE_H
#define CSTATE_H
#include "../../globals/globals.h"
class cState {
protected:
    cState(const string name) : STATE_NAME(name) {};
public:
    const string STATE_NAME;
    virtual void update() {};
    virtual void render() {
        Window.clear();
        Window.display();
    };
    virtual void create() {};
    virtual void destroy() {};
};
#endif // CSTATE_H
