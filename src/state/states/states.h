/**
    states.h is a namespace which includes all game states.
**/
#ifndef STATES_H
#define STATES_H

#include "test/state_TEST.h"
#include "test/state_TEST_OPTION.h"
#include "test/state_TEST_END.h"
#include "main_menu/state_MAIN_MENU.h"
#include "default/state_DEFAULT.h"
#include "credits/state_CREDITS.h"

#endif // STATES_H
