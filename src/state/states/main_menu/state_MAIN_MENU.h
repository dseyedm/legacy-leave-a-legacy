#ifndef STATE_MAIN_MENU_H
#define STATE_MAIN_MENU_H
#include "../../../globals/globals.h"
#include "../states.h"
#include "../../cState/cState.h"
namespace STATE {
/**
    state MAIN_MENU is the main state of the program - it displays several buttons
    that act as 'links' to other states.
**/
class MAIN_MENU : public cState {
private:
    MAIN_MENU() : cState("MAIN_MENU") {};
    static MAIN_MENU* instance;

    // sprite and texture for background
    sf::Texture tBackground;
    sf::Sprite sBackground;

    // font and texture for the buttons
    sf::Font fGentium;
    sf::Texture tMenuButton;

    static void PlayOnHover();
    static void GotoTestOption();
    static void GotoCredits();
    static void GotoExit();
public:
    static MAIN_MENU* INSTANCE();

    void update();
    void render();
    void create();
    void destroy();
};
};
#endif // STATE_MAIN_MENU_H
