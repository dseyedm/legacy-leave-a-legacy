#include "state_MAIN_MENU.h"
#include "../../../gui/cGUIButton.h"
// MAIN_MENU
STATE::MAIN_MENU *STATE::MAIN_MENU::instance = NULL;
STATE::MAIN_MENU *STATE::MAIN_MENU::INSTANCE() {
    if(instance == NULL) {
        instance = new MAIN_MENU;
    }
    return instance;
}

void STATE::MAIN_MENU::update() {
    cGUIManager::getInstance()->updateElements();
}
void STATE::MAIN_MENU::render() {
    Window.clear(sf::Color(60,60,60));
    Window.draw(sBackground);
    cGUIManager::getInstance()->renderElements();
    Window.display();
}
void STATE::MAIN_MENU::create() {
    // load the background
    if(!tBackground.loadFromFile(FILEPATH::TEXTURE::BACKGROUND_MAINMENU)) {
        // log it and exit
        cLogManager::getInstance()->log(ERR::NOLOAD::TEXTURE::BACKGROUND_MAINMENU,LOG::DIR,LOG::FILE);
        cErrorManager::getInstance()->displayError(ERR::NOLOAD::TEXTURE::BACKGROUND_MAINMENU,LOG::DIR,LOG::FILE);
    }
    sBackground.setTexture(tBackground);

    // load the buttons
    if(!fGentium.loadFromFile(FILEPATH::FONT::GENTIUM)) {
        // log it and exit
        cLogManager::getInstance()->log(ERR::NOLOAD::FONT::GENTIUM,LOG::DIR,LOG::FILE);
        cErrorManager::getInstance()->displayError(ERR::NOLOAD::FONT::GENTIUM,LOG::DIR,LOG::FILE);
    }
    if(!tMenuButton.loadFromFile(FILEPATH::TEXTURE::MENUBUTTON)) {
        // log it and exit
        cLogManager::getInstance()->log(ERR::NOLOAD::TEXTURE::MENUBUTTON,LOG::DIR,LOG::FILE);
        cErrorManager::getInstance()->displayError(ERR::NOLOAD::TEXTURE::MENUBUTTON,LOG::DIR,LOG::FILE);
    }

    cGUIManager::getInstance()->createElement(new cGUIButton("button1"));
    cGUIButton *button1 = dynamic_cast<cGUIButton *>(cGUIManager::getInstance()->getElement("button1"));
    button1->setString("Start");
    button1->Text.setFont(fGentium);
    button1->Text.setColor(sf::Color(65,65,65));
    button1->Shape.setTexture(&tMenuButton,true);
    button1->Normal = sf::Color(240,240,240);
    button1->Tinted = sf::Color(255,255,255);
    button1->Shape.setFillColor(button1->Normal);
    button1->setPosition(sf::Vector2f(Window.getSize().x/2-button1->Shape.getSize().x/2,Window.getSize().y/2-button1->Shape.getSize().y/2));
    button1->setClickCallBack(GotoTestOption);
    button1->setMouseInCallBack(PlayOnHover);

    cGUIManager::getInstance()->createElement(new cGUIButton("button2"));
    cGUIButton *button2 = dynamic_cast<cGUIButton *>(cGUIManager::getInstance()->getElement("button2"));
    button2->setString("Credits");
    button2->Text.setFont(fGentium);
    button2->Text.setColor(sf::Color(65,65,65));
    button2->Shape.setTexture(&tMenuButton,true);
    button2->Normal = sf::Color(240,240,240);
    button2->Tinted = sf::Color(255,255,255);
    button2->Shape.setFillColor(button2->Normal);
    button2->setPosition(sf::Vector2f(button1->Shape.getPosition().x,
                                      button1->Shape.getPosition().y+button1->Shape.getLocalBounds().height+5));
    button2->setClickCallBack(GotoCredits);
    button2->setMouseInCallBack(PlayOnHover);

    cGUIManager::getInstance()->createElement(new cGUIButton("button3"));
    cGUIButton *button3 = dynamic_cast<cGUIButton *>(cGUIManager::getInstance()->getElement("button3"));
    button3->setString("Exit");
    button3->Text.setFont(fGentium);
    button3->Text.setColor(sf::Color(65,65,65));
    button3->Shape.setTexture(&tMenuButton,true);
    button3->Normal = sf::Color(240,240,240);
    button3->Tinted = sf::Color(255,255,255);
    button3->Shape.setFillColor(button3->Normal);
    button3->setPosition(sf::Vector2f(button2->Shape.getPosition().x,
                                      button2->Shape.getPosition().y+button1->Shape.getLocalBounds().height+5));
    button3->setClickCallBack(GotoExit);
    button3->setMouseInCallBack(PlayOnHover);
}
void STATE::MAIN_MENU::destroy() {
    cSoundManager::getInstance()->Player.setVolume(100);
    // destroy all gui elements
    cGUIManager::getInstance()->destroyAllElements();
    delete instance;
    instance = NULL;
}
void STATE::MAIN_MENU::PlayOnHover() {
    cSoundManager::getInstance()->Player.setVolume(35);
    cSoundManager::getInstance()->play(FILEPATH::SOUND::CLICK_3);
}
void STATE::MAIN_MENU::GotoTestOption() {
    cSoundManager::getInstance()->Player.setVolume(100);
    cSoundManager::getInstance()->play(FILEPATH::SOUND::CLICK_4);
    cStateManager::getInstance()->setState(STATE::TEST_OPTION::INSTANCE());
}
void STATE::MAIN_MENU::GotoCredits() {
    cSoundManager::getInstance()->Player.setVolume(100);
    cSoundManager::getInstance()->play(FILEPATH::SOUND::CLICK_4);
    cStateManager::getInstance()->setState(STATE::CREDITS::INSTANCE());
}
void STATE::MAIN_MENU::GotoExit() {
    cSoundManager::getInstance()->Player.setVolume(100);
    cSoundManager::getInstance()->play(FILEPATH::SOUND::CLICK_4);
    Window.close();
};
