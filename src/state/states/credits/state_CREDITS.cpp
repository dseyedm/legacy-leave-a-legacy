#include "state_CREDITS.h"
#include "../../../gui/cGUIButton.h"
// CREDITS
STATE::CREDITS *STATE::CREDITS::instance = NULL;
STATE::CREDITS *STATE::CREDITS::INSTANCE() {
    if(instance == NULL) {
        instance = new CREDITS;
    }
    return instance;
}

void STATE::CREDITS::update() {
    if(CreditRollTime.getElapsedTime().asSeconds() > 60.f) {
        CreditRollTime.restart();
    }

    CreditRollHeight = 45.f * CreditRollTime.getElapsedTime().asSeconds();

    Credits.setPosition(200.f, Window.getSize().y - CreditRollHeight);

    cGUIManager::getInstance()->updateElements();
}

void STATE::CREDITS::render() {
    Window.clear(sf::Color(60,60,60));
    Window.draw(sBackground);
    Window.draw(Credits);
    Window.draw(sOverlay);
    cGUIManager::getInstance()->renderElements();
    Window.display();
}

void STATE::CREDITS::create() {
    // load the background
    if(!tBackground.loadFromFile(FILEPATH::TEXTURE::BACKGROUND_CREDITS)) {
        // log it and exit
        cLogManager::getInstance()->log(ERR::NOLOAD::TEXTURE::BACKGROUND_CREDITS,LOG::DIR,LOG::FILE);
        cErrorManager::getInstance()->displayError(ERR::NOLOAD::TEXTURE::BACKGROUND_CREDITS,LOG::DIR,LOG::FILE);
    }
    sBackground.setTexture(tBackground);

    // load overlay
    if(!tOverlay.loadFromFile(FILEPATH::TEXTURE::CREDITS_OVERLAY)) {
        // log it and exit
        cLogManager::getInstance()->log(ERR::NOLOAD::TEXTURE::CREDITS_OVERLAY,LOG::DIR,LOG::FILE);
        cErrorManager::getInstance()->displayError(ERR::NOLOAD::TEXTURE::CREDITS_OVERLAY,LOG::DIR,LOG::FILE);
    }
    sOverlay.setTexture(tOverlay);

    // load the button and credit font
    if(!fGentium.loadFromFile(FILEPATH::FONT::GENTIUM)) {
        // log it and exit
        cLogManager::getInstance()->log(ERR::NOLOAD::FONT::GENTIUM,LOG::DIR,LOG::FILE);
        cErrorManager::getInstance()->displayError(ERR::NOLOAD::FONT::GENTIUM,LOG::DIR,LOG::FILE);
    }
    // load the button texture
    if(!tMenuButton.loadFromFile(FILEPATH::TEXTURE::MENUBUTTON)) {
        // log it and exit
        cLogManager::getInstance()->log(ERR::NOLOAD::TEXTURE::MENUBUTTON,LOG::DIR,LOG::FILE);
        cErrorManager::getInstance()->displayError(ERR::NOLOAD::TEXTURE::MENUBUTTON,LOG::DIR,LOG::FILE);
    }

    // intialize credit roll
    Credits.setString("Created by\
                      \n  Soraj Seyed Mahmoud\
                      \n  Nathan Wiebe-Neufeldt\
                      \n\
                      \nSoftware Used\
                      \n  CodeBlocks 10.05\
                      \n  SFML 2.0\
                      \n  GIMP 2.6\
                      \n\
                      \nResources\
                      \n  FreeSound.co.uk\
                      \n  iStockPhoto.com\
                      \n  DaFont.com\
                      \n\
                      \n  All resources used\
                      \n  with permission");
    Credits.setFont(fGentium);
    Credits.setCharacterSize(36);
    Credits.setColor(sf::Color(65,65,65));

    // back button
    cGUIManager::getInstance()->createElement(new cGUIButton("backbutton"));
    cGUIButton *buttonBack = dynamic_cast<cGUIButton *>(cGUIManager::getInstance()->getElement("backbutton"));
    buttonBack->setString("Back");
    buttonBack->Text.setFont(fGentium);
    buttonBack->Text.setColor(sf::Color(65,65,65));
    buttonBack->Shape.setTexture(&tMenuButton,true);
    buttonBack->Normal = sf::Color(240,240,240);
    buttonBack->Tinted = sf::Color(255,255,255);
    buttonBack->Shape.setFillColor(buttonBack->Normal);
    buttonBack->setPosition(sf::Vector2f(25,510));
    buttonBack->setClickCallBack(GotoMainMenu);
    buttonBack->setMouseInCallBack(PlayOnHover);

    CreditRollTime.restart();
    CreditRollHeight = 0.f;
}

void STATE::CREDITS::destroy() {
    cGUIManager::getInstance()->destroyAllElements();
    delete instance;
    instance = NULL;
}

void STATE::CREDITS::GotoMainMenu() {
    cSoundManager::getInstance()->Player.setVolume(100);
    cSoundManager::getInstance()->play(FILEPATH::SOUND::CLICK_4);
    cStateManager::getInstance()->setState(STATE::MAIN_MENU::INSTANCE());
}
// should be playOnHover, but this is a little hack to get past the multiple definition errors
void STATE::CREDITS::PlayOnHover() {
    cSoundManager::getInstance()->Player.setVolume(35);
    cSoundManager::getInstance()->play(FILEPATH::SOUND::CLICK_3);
}
