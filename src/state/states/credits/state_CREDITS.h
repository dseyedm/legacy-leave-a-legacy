#ifndef STATE_CREDITS_H
#define STATE_CREDITS_H
#include "../../../globals/globals.h"
#include "../states.h"
#include "../../cState/cState.h"
namespace STATE {
/**
    state CREDITS is for naming the authors of the project
**/
class CREDITS : public cState {
private:
    CREDITS() : cState("CREDITS") {};
    static CREDITS* instance;

    sf::Texture tBackground;
    sf::Sprite sBackground;
    sf::Texture tOverlay;
    sf::Sprite sOverlay;

    sf::Font fGentium;
    sf::Texture tMenuButton;
    static void GotoMainMenu();
    static void PlayOnHover();

    sf::Clock CreditRollTime;
    float CreditRollHeight;

    sf::Text Credits;

    sf::Texture tSFML_Logo;
    sf::Sprite sSFML_Logo;

public:
    static CREDITS* INSTANCE();

    void create();
    void update();
    void render();
    void destroy();
};
};
#endif // STATE_CREDITS_H
