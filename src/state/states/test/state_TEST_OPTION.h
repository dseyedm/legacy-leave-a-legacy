#ifndef STATE_TEST_OPTION_H
#define STATE_TEST_OPTION_H
#include "../../../globals/globals.h"
#include "../states.h"
#include "../../cState/cState.h"
namespace STATE {
/**
    state TEST_OPTION is the options menu before entering the actual test.
**/
class TEST_OPTION : public cState {
private:
    TEST_OPTION() : cState("TEST_OPTION") {};
    static TEST_OPTION* instance;

    // background
    sf::Texture tBackground;
    sf::Sprite sBackground;
    // font
    sf::Font fGentium;
    sf::Texture tMenuButton;
    static sf::Texture tMenuCheckboxBlank;
    static sf::Texture tMenuCheckboxMarked;
    static void UpdateCheckboxTextures();
    static void GotoMainMenu();
    static void PlayOnHover();
    static void GotoTest();
public:
    static TEST_OPTION* INSTANCE();
    void create();
    void destroy();

    void update();
    void render();
};
};
#endif // STATE_TEST_OPTION_H
