#include "state_TEST.h"
#include "../../../gui/cGUIButton.h"
#include "../../../gui/cGUIBar.h"
// TEST
STATE::TEST *STATE::TEST::instance = NULL;
STATE::TEST *STATE::TEST::INSTANCE() {
    if(instance == NULL) {
        instance = new TEST;
    }
    return instance;
}

void STATE::TEST::update() {
    {
        // TIMER
        cGUIBar *bar1 = dynamic_cast<cGUIBar *>(cGUIManager::getInstance()->getElement("bar1"));    // timer bar
        stringstream ss;
        int tminutes = (int)Timer.getElapsedTime().asSeconds()/60;
        int tseconds = (int)Timer.getElapsedTime().asSeconds()%60;
        string buffer = "Time: ";
        if(tminutes >= 1) {
            ss << tminutes;
            buffer.append(ss.str());
            ss.str("");
            ss << tseconds;
            if(tseconds < 10)buffer.append(":0" + ss.str());
            else buffer.append(":" + ss.str());
        } else {
            ss << tseconds;
            buffer.append(ss.str());
        }
        bar1->setString(buffer);
    }

    {
        // CurrentQuestion/TotalQuestions
        cGUIBar *bar2 = dynamic_cast<cGUIBar *>(cGUIManager::getInstance()->getElement("bar2"));    // CurrentQuestion/TotalQuestions bar
        stringstream ss;
        ss.str("");
        ss << CurrentQuestion;
        string buffer = ss.str();
        ss.str("");
        ss << TotalQuestions;
        buffer.append(" / " + ss.str());
        bar2->setString(buffer);
    }

    {
        // Current subject
        cGUIBar *bar3 = dynamic_cast<cGUIBar *>(cGUIManager::getInstance()->getElement("bar3"));    // Subject
        if(CurrentSubject == SUBJECT::MACBETH) {
            bar3->setString("Macbeth");
        } else if(CurrentSubject == SUBJECT::LOTF) {
            bar3->setString("Lord of the Flies");
        } else if(CurrentSubject == SUBJECT::DOLLS_HOUSE) {
            bar3->setString("Doll's house");
        } else if(CurrentSubject == SUBJECT::TERMS) {
            bar3->setString("Terms");
        } else if(CurrentSubject == SUBJECT::THE_FLY) {
            bar3->setString("The Fly");
        } else if(CurrentSubject == SUBJECT::THE_GIRAFFE) {
            bar3->setString("The Giraffe");
        } else if(CurrentSubject == SUBJECT::AFTER_YOU) {
            bar3->setString("After you my Dear Alphonse");
        } else if(CurrentSubject == SUBJECT::BROTHER_DEATH) {
            bar3->setString("Brother Death");
        } else if(CurrentSubject == SUBJECT::BUTCHER_BIRD) {
            bar3->setString("Butcher Bird");
        } else if(CurrentSubject == SUBJECT::SECRET_LIFE) {
            bar3->setString("Secret Life of Walter Mitty");
        }
    }

    {
        //percentage
        cGUIBar *bar4 = dynamic_cast<cGUIBar *>(cGUIManager::getInstance()->getElement("bar4"));    // percentage

        float percentage = 0;
        float right = QuestionsRight;
        float total = CurrentQuestion;
        if(CurrentQuestion > 0) {
            percentage = (right/total) * 100;
        }
        stringstream ss;
        ss << (int)percentage;
        bar4->setString(ss.str() + " %");
    }

    cGUIManager::getInstance()->updateElements();
}
void STATE::TEST::render() {
    Window.clear();
    Window.draw(sBackground);
    cGUIManager::getInstance()->renderElements();
    Window.draw(sQuestion);
    Window.draw(sAnswerA);
    Window.draw(sAnswerB);
    Window.draw(sAnswerC);
    Window.draw(sAnswerD);
    Window.draw(isCorrect);
    Window.display();
}

void STATE::TEST::create() {
    // load the background
    if(!tBackground.loadFromFile(FILEPATH::TEXTURE::BACKGROUND_TEST)) {
        // log it and exit
        cLogManager::getInstance()->log(ERR::NOLOAD::TEXTURE::BACKGROUND_TEST,LOG::DIR,LOG::FILE);
        cErrorManager::getInstance()->displayError(ERR::NOLOAD::TEXTURE::BACKGROUND_TEST,LOG::DIR,LOG::FILE);
    }
    sBackground.setTexture(tBackground);

    // load the buttons
    if(!tMenuButton.loadFromFile(FILEPATH::TEXTURE::MENUBUTTON)) {
        // log it and exit
        cLogManager::getInstance()->log(ERR::NOLOAD::TEXTURE::MENUBUTTON,LOG::DIR,LOG::FILE);
        cErrorManager::getInstance()->displayError(ERR::NOLOAD::TEXTURE::MENUBUTTON,LOG::DIR,LOG::FILE);
    }
    if(!tMenuBar.loadFromFile(FILEPATH::TEXTURE::MENUBAR)) {
        // log it and exit
        cLogManager::getInstance()->log(ERR::NOLOAD::TEXTURE::MENUBAR,LOG::DIR,LOG::FILE);
        cErrorManager::getInstance()->displayError(ERR::NOLOAD::TEXTURE::MENUBAR,LOG::DIR,LOG::FILE);
    }
    if(!fGentium.loadFromFile(FILEPATH::FONT::GENTIUM)) {
        // log it and exit
        cLogManager::getInstance()->log(ERR::NOLOAD::FONT::GENTIUM,LOG::DIR,LOG::FILE);
        cErrorManager::getInstance()->displayError(ERR::NOLOAD::FONT::GENTIUM,LOG::DIR,LOG::FILE);
    }

    // now create each button/bar with cGUIManager
    cGUIManager::getInstance()->createElement(new cGUIBar("bar1"));
    cGUIBar *bar1 = dynamic_cast<cGUIBar *>(cGUIManager::getInstance()->getElement("bar1"));
    bar1->setString("Timer");
    bar1->Text.setCharacterSize(28);
    bar1->Text.setFont(fGentium);
    bar1->Text.setColor(sf::Color(65,65,65));
    bar1->Shape.setTexture(&tMenuBar,true);
    bar1->Normal = sf::Color(255,255,255);
    bar1->Shape.setFillColor(bar1->Normal);
    bar1->setPosition(sf::Vector2f(5,5));
    bar1->setSize(sf::Vector2f(150,50));

    cGUIManager::getInstance()->createElement(new cGUIBar("bar2"));
    cGUIBar *bar2 = dynamic_cast<cGUIBar *>(cGUIManager::getInstance()->getElement("bar2"));
    bar2->setString("15/25");
    bar2->Text.setCharacterSize(25);
    bar2->Text.setFont(fGentium);
    bar2->Text.setColor(sf::Color(65,65,65));
    bar2->Shape.setTexture(&tMenuBar,true);
    bar2->Normal = sf::Color(255,255,255);
    bar2->Shape.setFillColor(bar2->Normal);
    bar2->setPosition(sf::Vector2f(800-150-5,5));
    bar2->setSize(sf::Vector2f(150,50));

    cGUIManager::getInstance()->createElement(new cGUIBar("bar3"));
    cGUIBar *bar3 = dynamic_cast<cGUIBar *>(cGUIManager::getInstance()->getElement("bar3"));
    bar3->setString("Macbeth");
    bar3->Text.setCharacterSize(28);
    bar3->Text.setFont(fGentium);
    bar3->Text.setColor(sf::Color(65,65,65));
    bar3->Shape.setTexture(&tMenuBar,true);
    bar3->Normal = sf::Color(255,255,255);
    bar3->Shape.setFillColor(bar3->Normal);
    bar3->setSize(sf::Vector2f(390,60));
    bar3->setPosition(sf::Vector2f(Window.getSize().x/2-bar3->Shape.getSize().x/2,5));

    cGUIManager::getInstance()->createElement(new cGUIBar("bar4"));
    cGUIBar *bar4 = dynamic_cast<cGUIBar *>(cGUIManager::getInstance()->getElement("bar4"));
    bar4->setString("percentage");
    bar4->Text.setCharacterSize(28);
    bar4->Text.setFont(fGentium);
    bar4->Text.setColor(sf::Color(65,65,65));
    bar4->Shape.setTexture(&tMenuBar,true);
    bar4->Normal = sf::Color(255,255,255);
    bar4->Shape.setFillColor(bar4->Normal);
    bar4->setSize(sf::Vector2f(200,50));
    bar4->setPosition(sf::Vector2f(Window.getSize().x/2-bar4->Shape.getSize().x/2,540));

    // back button
    cGUIManager::getInstance()->createElement(new cGUIButton("button1"));
    cGUIButton *button1 = dynamic_cast<cGUIButton *>(cGUIManager::getInstance()->getElement("button1"));
    button1->setString("Exit");
    button1->Text.setFont(fGentium);
    button1->Text.setColor(sf::Color(65,65,65));
    button1->Shape.setTexture(&tMenuButton,true);
    button1->Normal = sf::Color(240,240,240);
    button1->Tinted = sf::Color(255,255,255);
    button1->Shape.setFillColor(button1->Normal);
    button1->setPosition(sf::Vector2f(5,535));
    button1->setSize(sf::Vector2f(150,50));
    button1->setClickCallBack(GotoMainMenu);
    button1->setMouseInCallBack(PlayOnHover);

    // next button
    cGUIManager::getInstance()->createElement(new cGUIButton("button2"));
    cGUIButton *button2 = dynamic_cast<cGUIButton *>(cGUIManager::getInstance()->getElement("button2"));
    button2->Text.setFont(fGentium);
    button2->Text.setColor(sf::Color(65,65,65));
    button2->Shape.setTexture(&tMenuButton,true);
    button2->Normal = sf::Color(240,240,240);
    button2->Tinted = sf::Color(255,255,255);
    button2->Shape.setFillColor(button2->Normal);
    button2->setPosition(sf::Vector2f(800-150-5,540));
    button2->setSize(sf::Vector2f(150,50));
    button2->setClickCallBack(NextQuestion);
    button2->setMouseInCallBack(PlayOnHover);
    button2->Text.setString("Verify");

    /**
        set up the test
    **/
    NewQuestion();
    sQuestion.setFont(fGentium);
    sQuestion.setColor(sf::Color(65,65,65));
    sQuestion.setPosition(30,135);
    sQuestion.setCharacterSize(23);

    CurrentQuestion = 0;    // start of test
    ChosenAnswer = 0;   // NULL
    QuestionsRight = 0;     // start of test

    // make four ABCD buttons
    cGUIManager::getInstance()->createElement(new cGUIButton("A"));
    cGUIButton *A = dynamic_cast<cGUIButton *>(cGUIManager::getInstance()->getElement("A"));
    A->Text.setString("A");
    A->Text.setColor(sf::Color(65,65,65));
    A->Text.setFont(fGentium);
    A->Shape.setTexture(&tMenuButton,true);
    A->Normal = sf::Color(240,240,240);
    A->Tinted = sf::Color(255,255,255);
    A->Shape.setFillColor(A->Normal);
    A->setPosition(sf::Vector2f(30,200));
    A->setSize(sf::Vector2f(50,50));
    A->setClickCallBack(ChooseA);
    A->setMouseInCallBack(PlayOnHover);

    sAnswerA.setFont(fGentium);
    sAnswerA.setColor(sf::Color(65,65,65));
    sAnswerA.setPosition(100,A->Shape.getPosition().y);
    sAnswerA.setCharacterSize(23);

    cGUIManager::getInstance()->createElement(new cGUIButton("B"));
    cGUIButton *B = dynamic_cast<cGUIButton *>(cGUIManager::getInstance()->getElement("B"));
    B->Text.setString("B");
    B->Text.setColor(sf::Color(65,65,65));
    B->Text.setFont(fGentium);
    B->Shape.setTexture(&tMenuButton,true);
    B->Normal = sf::Color(240,240,240);
    B->Tinted = sf::Color(255,255,255);
    B->Shape.setFillColor(B->Normal);
    B->setPosition(sf::Vector2f(30,A->Shape.getPosition().y+A->Shape.getSize().y+10));
    B->setSize(sf::Vector2f(50,50));
    B->setClickCallBack(ChooseB);
    B->setMouseInCallBack(PlayOnHover);

    sAnswerB.setFont(fGentium);
    sAnswerB.setColor(sf::Color(65,65,65));
    sAnswerB.setPosition(100,B->Shape.getPosition().y);
    sAnswerB.setCharacterSize(23);

    cGUIManager::getInstance()->createElement(new cGUIButton("C"));
    cGUIButton *C = dynamic_cast<cGUIButton *>(cGUIManager::getInstance()->getElement("C"));
    C->Text.setString("C");
    C->Text.setColor(sf::Color(65,65,65));
    C->Text.setFont(fGentium);
    C->Shape.setTexture(&tMenuButton,true);
    C->Normal = sf::Color(240,240,240);
    C->Tinted = sf::Color(255,255,255);
    C->Shape.setFillColor(C->Normal);
    C->setPosition(sf::Vector2f(30,B->Shape.getPosition().y+B->Shape.getSize().y+10));
    C->setSize(sf::Vector2f(50,50));
    C->setClickCallBack(ChooseC);
    C->setMouseInCallBack(PlayOnHover);

    sAnswerC.setFont(fGentium);
    sAnswerC.setColor(sf::Color(65,65,65));
    sAnswerC.setPosition(100,C->Shape.getPosition().y);
    sAnswerC.setCharacterSize(23);

    cGUIManager::getInstance()->createElement(new cGUIButton("D"));
    cGUIButton *D = dynamic_cast<cGUIButton *>(cGUIManager::getInstance()->getElement("D"));
    D->Text.setString("D");
    D->Text.setColor(sf::Color(65,65,65));
    D->Text.setFont(fGentium);
    D->Shape.setTexture(&tMenuButton,true);
    D->Normal = sf::Color(240,240,240);
    D->Tinted = sf::Color(255,255,255);
    D->Shape.setFillColor(D->Normal);
    D->setPosition(sf::Vector2f(30,C->Shape.getPosition().y+C->Shape.getSize().y+10));
    D->setSize(sf::Vector2f(50,50));
    D->setClickCallBack(ChooseD);
    D->setMouseInCallBack(PlayOnHover);

    sAnswerD.setFont(fGentium);
    sAnswerD.setColor(sf::Color(65,65,65));
    sAnswerD.setPosition(100,D->Shape.getPosition().y);
    sAnswerD.setCharacterSize(23);

    isCorrect.setFont(fGentium);
    isCorrect.setPosition(10,450);
    isCorrect.setString("");
    isCorrect.setCharacterSize(30);
    isCorrect.setStyle(sf::Text::Bold);
}
void STATE::TEST::destroy() {
    cSoundManager::getInstance()->Player.setVolume(100);
    // destroy all gui elements
    cGUIManager::getInstance()->destroyAllElements();
    delete instance;
    instance = NULL;
}

void STATE::TEST::GotoMainMenu() {
    cSoundManager::getInstance()->Player.setVolume(100);
    cSoundManager::getInstance()->play(FILEPATH::SOUND::CLICK_4);
    cStateManager::getInstance()->setState(STATE::MAIN_MENU::INSTANCE());
}
void STATE::TEST::NextQuestion() {
    static bool AnswerShown = false;
    static bool AnswerShownLast = false;
    // play click sound
    cSoundManager::getInstance()->Player.setVolume(100);
    cSoundManager::getInstance()->play(FILEPATH::SOUND::CLICK_4);

    // if he has chosen abcd
    if(instance->ChosenAnswer != 0) {
        // change button to 'next'
        cGUIButton *next = dynamic_cast<cGUIButton *>(cGUIManager::getInstance()->getElement("button2"));
        next->setString("Next");
        // check if it is the right answer
        if(instance->ChosenAnswer == instance->CorrectAnswer) {
            // CORRECT ANSWER
            instance->isCorrect.setColor(sf::Color(100,200,100));
            instance->isCorrect.setString("Correct!");
        } else {
            // WRONG ANSWER
            instance->isCorrect.setColor(sf::Color(200,100,100));
            string answer = "?";
            if(instance->CorrectAnswer == 1) {
                answer = "A";
            } else if(instance->CorrectAnswer == 2) {
                answer = "B";
            } else if(instance->CorrectAnswer == 3) {
                answer = "C";
            } else if(instance->CorrectAnswer == 4) {
                answer = "D";
            }
            instance->isCorrect.setString("Wrong, the answer was " + answer);
        }

        if(AnswerShown) {
            AnswerShown = false;
            AnswerShownLast = true;
            // if the test hasn't finished
            if(instance->CurrentQuestion >= instance->TotalQuestions) {
                // test has finished, show mark
                // first percentage
                float right = instance->QuestionsRight;
                float total = instance->CurrentQuestion;

                // now the timer
                stringstream ss;
                int tminutes = (int)instance->Timer.getElapsedTime().asSeconds()/60;
                int tseconds = (int)instance->Timer.getElapsedTime().asSeconds()%60;
                string buffer = "";
                if(tminutes >= 1) {
                    ss << tminutes;
                    buffer.append(ss.str());
                    ss.str("");
                    ss << tseconds;
                    if(tseconds < 10)buffer.append(":0" + ss.str());
                    else buffer.append(":" + ss.str());
                } else {
                    ss << tseconds;
                    buffer.append(ss.str());
                }

                STATE::TEST_END::INSTANCE()->setParameters((right/total)*100,instance->TotalQuestions,buffer);
                cStateManager::getInstance()->setState(STATE::TEST_END::INSTANCE());
            } else {
                if(instance->ChosenAnswer == instance->CorrectAnswer) {
                    instance->QuestionsRight++;
                }
                // set the next answer to NULl
                instance->ChosenAnswer = 0;
                instance->CurrentQuestion++;

                // unhighlight everything
                cGUIButton *A = dynamic_cast<cGUIButton *>(cGUIManager::getInstance()->getElement("A"));
                A->Normal = sf::Color(240,240,240);
                A->Shape.setFillColor(A->Normal);

                cGUIButton *B = dynamic_cast<cGUIButton *>(cGUIManager::getInstance()->getElement("B"));
                B->Normal = sf::Color(240,240,240);
                B->Shape.setFillColor(B->Normal);

                cGUIButton *C = dynamic_cast<cGUIButton *>(cGUIManager::getInstance()->getElement("C"));
                C->Normal = sf::Color(240,240,240);
                C->Shape.setFillColor(C->Normal);

                cGUIButton *D = dynamic_cast<cGUIButton *>(cGUIManager::getInstance()->getElement("D"));
                D->Normal = sf::Color(240,240,240);
                D->Shape.setFillColor(D->Normal);
                // load a new question
                instance->NewQuestion();
                // change button to 'next'
                cGUIButton *next = dynamic_cast<cGUIButton *>(cGUIManager::getInstance()->getElement("button2"));
                next->Text.setString("Verify");
                instance->isCorrect.setString("");
            }
        }
        if(!AnswerShownLast)AnswerShown = true;
        else AnswerShownLast = false;
    } else {
        // play error sound
        cSoundManager::getInstance()->Player.setVolume(50);
        cSoundManager::getInstance()->play(FILEPATH::SOUND::ERROR_1);
    }
}
void STATE::TEST::PlayOnHover() {
    cSoundManager::getInstance()->Player.setVolume(35);
    cSoundManager::getInstance()->play(FILEPATH::SOUND::CLICK_3);
}

void STATE::TEST::ChooseA() {
    cGUIButton *next = dynamic_cast<cGUIButton *>(cGUIManager::getInstance()->getElement("button2"));
    if(next->Text.getString() != "Next") {
        instance->ChosenAnswer = 1;
        // highlight A but nothing else
        cGUIButton *A = dynamic_cast<cGUIButton *>(cGUIManager::getInstance()->getElement("A"));
        A->Normal = sf::Color(255,255,255);
        A->Shape.setFillColor(A->Normal);

        cGUIButton *B = dynamic_cast<cGUIButton *>(cGUIManager::getInstance()->getElement("B"));
        B->Normal = sf::Color(240,240,240);
        B->Shape.setFillColor(B->Normal);

        cGUIButton *C = dynamic_cast<cGUIButton *>(cGUIManager::getInstance()->getElement("C"));
        C->Normal = sf::Color(240,240,240);
        C->Shape.setFillColor(C->Normal);

        cGUIButton *D = dynamic_cast<cGUIButton *>(cGUIManager::getInstance()->getElement("D"));
        D->Normal = sf::Color(240,240,240);
        D->Shape.setFillColor(D->Normal);
    }
}
void STATE::TEST::ChooseB() {
    cGUIButton *next = dynamic_cast<cGUIButton *>(cGUIManager::getInstance()->getElement("button2"));
    if(next->Text.getString() != "Next") {
        instance->ChosenAnswer = 2;
        // highlight B but nothing else
        cGUIButton *A = dynamic_cast<cGUIButton *>(cGUIManager::getInstance()->getElement("A"));
        A->Normal = sf::Color(240,240,240);
        A->Shape.setFillColor(A->Normal);

        cGUIButton *B = dynamic_cast<cGUIButton *>(cGUIManager::getInstance()->getElement("B"));
        B->Normal = sf::Color(255,255,255);
        B->Shape.setFillColor(B->Normal);

        cGUIButton *C = dynamic_cast<cGUIButton *>(cGUIManager::getInstance()->getElement("C"));
        C->Normal = sf::Color(240,240,240);
        C->Shape.setFillColor(C->Normal);

        cGUIButton *D = dynamic_cast<cGUIButton *>(cGUIManager::getInstance()->getElement("D"));
        D->Normal = sf::Color(240,240,240);
        D->Shape.setFillColor(D->Normal);
    }
}
void STATE::TEST::ChooseC() {
    cGUIButton *next = dynamic_cast<cGUIButton *>(cGUIManager::getInstance()->getElement("button2"));
    if(next->Text.getString() != "Next") {
        instance->ChosenAnswer = 3;
        // highlight C but nothing else
        cGUIButton *A = dynamic_cast<cGUIButton *>(cGUIManager::getInstance()->getElement("A"));
        A->Normal = sf::Color(240,240,240);
        A->Shape.setFillColor(A->Normal);

        cGUIButton *B = dynamic_cast<cGUIButton *>(cGUIManager::getInstance()->getElement("B"));
        B->Normal = sf::Color(240,240,240);
        B->Shape.setFillColor(B->Normal);

        cGUIButton *C = dynamic_cast<cGUIButton *>(cGUIManager::getInstance()->getElement("C"));
        C->Normal = sf::Color(255,255,255);
        C->Shape.setFillColor(C->Normal);

        cGUIButton *D = dynamic_cast<cGUIButton *>(cGUIManager::getInstance()->getElement("D"));
        D->Normal = sf::Color(240,240,240);
        D->Shape.setFillColor(D->Normal);
    }
}
void STATE::TEST::ChooseD() {
    cGUIButton *next = dynamic_cast<cGUIButton *>(cGUIManager::getInstance()->getElement("button2"));
    if(next->Text.getString() != "Next") {
        instance->ChosenAnswer = 4;
        // highlight D but nothing else
        cGUIButton *A = dynamic_cast<cGUIButton *>(cGUIManager::getInstance()->getElement("A"));
        A->Normal = sf::Color(240,240,240);
        A->Shape.setFillColor(A->Normal);

        cGUIButton *B = dynamic_cast<cGUIButton *>(cGUIManager::getInstance()->getElement("B"));
        B->Normal = sf::Color(240,240,240);
        B->Shape.setFillColor(B->Normal);

        cGUIButton *C = dynamic_cast<cGUIButton *>(cGUIManager::getInstance()->getElement("C"));
        C->Normal = sf::Color(240,240,240);
        C->Shape.setFillColor(C->Normal);

        cGUIButton *D = dynamic_cast<cGUIButton *>(cGUIManager::getInstance()->getElement("D"));
        D->Normal = sf::Color(255,255,255);
        D->Shape.setFillColor(D->Normal);
    }
}

void STATE::TEST::NewQuestion() {
    // calculate test length on booleans
    TotalQuestions = 0;
    if(bMacbeth) {
        TotalQuestions += NUMBEROFQUESTIONS::NMACBETH;
    }
    if(bLotf) {
        TotalQuestions += NUMBEROFQUESTIONS::NLOTF;
    }
    if(bDolls) {
        TotalQuestions += NUMBEROFQUESTIONS::NDOLLS_HOUSE;
    }
    if(bTerms) {
        TotalQuestions += NUMBEROFQUESTIONS::NTERMS;
    }
    if(bFly) {
        TotalQuestions += NUMBEROFQUESTIONS::NTHE_FLY;
    }
    if(bGiraffe) {
        TotalQuestions += NUMBEROFQUESTIONS::NTHE_GIRAFFE;
    }
    if(bAfterYou) {
        TotalQuestions += NUMBEROFQUESTIONS::NAFTER_YOU;
    }
    if(bBrotherDeath) {
        TotalQuestions += NUMBEROFQUESTIONS::NBROTHER_DEATH;
    }
    if(bButcherBird) {
        TotalQuestions += NUMBEROFQUESTIONS::NBUTCHER_BIRD;
    }
    if(bSecretLife) {
        TotalQuestions += NUMBEROFQUESTIONS::NSECRET_LIFE;
    }

    // choose a subject based on booleans
    bool done = false;
    while(!done) {
        int random = rand() % 10;   // generate a random subject
        if(random == SUBJECT::MACBETH) {
            if(bMacbeth) {
                CurrentSubject = SUBJECT::MACBETH;
                done = true;
            }
        } else if(random == SUBJECT::LOTF) {
            if(bLotf) {
                CurrentSubject = SUBJECT::LOTF;
                done = true;
            }
        } else if(random == SUBJECT::DOLLS_HOUSE) {
            if(bDolls) {
                CurrentSubject = SUBJECT::DOLLS_HOUSE;
                done = true;
            }
        } else if(random == SUBJECT::TERMS) {
            if(bTerms) {
                CurrentSubject = SUBJECT::TERMS;
                done = true;
            }
        } else if(random == SUBJECT::THE_FLY) {
            if(bFly) {
                CurrentSubject = SUBJECT::THE_FLY;
                done = true;
            }
        } else if(random == SUBJECT::THE_GIRAFFE) {
            if(bGiraffe) {
                CurrentSubject = SUBJECT::THE_GIRAFFE;
                done = true;
            }
        } else if(random == SUBJECT::AFTER_YOU) {
            if(bAfterYou) {
                CurrentSubject = SUBJECT::AFTER_YOU;
                done = true;
            }
        } else if(random == SUBJECT::BROTHER_DEATH) {
            if(bBrotherDeath) {
                CurrentSubject = SUBJECT::BROTHER_DEATH;
                done = true;
            }
        } else if(random == SUBJECT::BUTCHER_BIRD) {
            if(bButcherBird) {
                CurrentSubject = SUBJECT::BUTCHER_BIRD;
                done = true;
            }
        } else if(random == SUBJECT::SECRET_LIFE) {
            if(bSecretLife) {
                CurrentSubject = SUBJECT::SECRET_LIFE;
                done = true;
            }
        }
    }

    // gets a random question from random subject
    Test.CurrentQuestion = Test.getRandomQuestion(CurrentSubject);
    sQuestion.setString(Test.CurrentQuestion->Question);

    bool doneA = false;
    bool doneB = false;
    bool doneC = false;
    bool doneD = false;

    int AnswerA = rand()%4+1;  // 1-4
    // set answer a
    if(AnswerA == 1) {
        sAnswerA.setString(Test.CurrentQuestion->AnswerA);
        doneA = true;
    } else if(AnswerA == 2) {
        sAnswerB.setString(Test.CurrentQuestion->AnswerA);
        doneB = true;
    } else if(AnswerA == 3) {
        sAnswerC.setString(Test.CurrentQuestion->AnswerA);
        doneC = true;
    } else if(AnswerA == 4) {
        sAnswerD.setString(Test.CurrentQuestion->AnswerA);
        doneD = true;
    }
    CorrectAnswer = AnswerA;

    bool AnswerB_used = false;
    bool AnswerC_used = false;
    bool AnswerD_used = false;

    if(!doneA) { // if button A has no question
        AnswerB_used = true;
        sAnswerA.setString(Test.CurrentQuestion->AnswerB);
    }
    if(!doneB) {
        if(!AnswerB_used) {
            AnswerB_used = true;
            sAnswerB.setString(Test.CurrentQuestion->AnswerB);
        } else {
            AnswerC_used = true;
            sAnswerB.setString(Test.CurrentQuestion->AnswerC);
        }
    }
    if(!doneC) {
        if(!AnswerB_used) {
            AnswerB_used = true;
            sAnswerC.setString(Test.CurrentQuestion->AnswerB);
        } else if(!AnswerC_used) {
            AnswerC_used = true;
            sAnswerC.setString(Test.CurrentQuestion->AnswerC);
        } else {
            AnswerD_used = true;
            sAnswerC.setString(Test.CurrentQuestion->AnswerD);
        }
    }
    if(!doneD) {
        if(!AnswerB_used) {
            AnswerB_used = true;
            sAnswerD.setString(Test.CurrentQuestion->AnswerB);
        } else if(!AnswerC_used) {
            AnswerC_used = true;
            sAnswerD.setString(Test.CurrentQuestion->AnswerC);
        } else if(!AnswerD_used) {
            AnswerD_used = true;
            sAnswerD.setString(Test.CurrentQuestion->AnswerD);
        } else {
            sAnswerD.setString("");
        }
    }
}
