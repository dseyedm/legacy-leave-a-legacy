#ifndef STATE_TEST_END_H
#define STATE_TEST_END_H
#include "../../../globals/globals.h"
#include "../states.h"
#include "../../cState/cState.h"
namespace STATE {
/**
    state TEST_END is when the test ends
**/
class TEST_END : public cState {
private:
    TEST_END() : cState("TEST_END") {};
    static TEST_END* instance;

    // background
    sf::Texture tBackground;
    sf::Sprite sBackground;
    // font
    sf::Font fGentium;
    sf::Texture tMenuButton;
    static void GotoMainMenu();
    static void PlayOnHover();

    sf::Text FinalMessage;
public:
    static TEST_END* INSTANCE();

    void setParameters(const float,const int,const string);

    void create();
    void destroy();

    void update();
    void render();
};
};
#endif // STATE_TEST_END_H
