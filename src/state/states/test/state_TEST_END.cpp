#include "state_TEST_END.h"
#include "../../../gui/cGUIButton.h"
#include "../../../gui/cGUICheckbox.h"
// TEST_END
STATE::TEST_END *STATE::TEST_END::instance = NULL;
STATE::TEST_END *STATE::TEST_END::INSTANCE() {
    if(instance == NULL) {
        instance = new TEST_END;
    }
    return instance;
}

void STATE::TEST_END::update() {
    cGUIManager::getInstance()->updateElements();
}
void STATE::TEST_END::render() {
    Window.clear();
    Window.draw(sBackground);
    Window.draw(FinalMessage);
    cGUIManager::getInstance()->renderElements();
    Window.display();
}

void STATE::TEST_END::create() {
    // load the background
    if(!tBackground.loadFromFile(FILEPATH::TEXTURE::BACKGROUND_TESTEND)) {
        // log it and exit
        cLogManager::getInstance()->log(ERR::NOLOAD::TEXTURE::BACKGROUND_TESTEND,LOG::DIR,LOG::FILE);
        cErrorManager::getInstance()->displayError(ERR::NOLOAD::TEXTURE::BACKGROUND_TESTEND,LOG::DIR,LOG::FILE);
    }
    sBackground.setTexture(tBackground);
    // load the button font
    if(!fGentium.loadFromFile(FILEPATH::FONT::GENTIUM)) {
        // log it and exit
        cLogManager::getInstance()->log(ERR::NOLOAD::FONT::GENTIUM,LOG::DIR,LOG::FILE);
        cErrorManager::getInstance()->displayError(ERR::NOLOAD::FONT::GENTIUM,LOG::DIR,LOG::FILE);
    }
    // load the button texture
    if(!tMenuButton.loadFromFile(FILEPATH::TEXTURE::MENUBUTTON)) {
        // log it and exit
        cLogManager::getInstance()->log(ERR::NOLOAD::TEXTURE::MENUBUTTON,LOG::DIR,LOG::FILE);
        cErrorManager::getInstance()->displayError(ERR::NOLOAD::TEXTURE::MENUBUTTON,LOG::DIR,LOG::FILE);
    }

    // back button
    cGUIManager::getInstance()->createElement(new cGUIButton("back"));
    cGUIButton *back = dynamic_cast<cGUIButton *>(cGUIManager::getInstance()->getElement("back"));
    back->setString("Main menu");
    back->Text.setFont(fGentium);
    back->Text.setColor(sf::Color(65,65,65));
    back->Shape.setTexture(&tMenuButton,true);
    back->Normal = sf::Color(240,240,240);
    back->Tinted = sf::Color(255,255,255);
    back->Shape.setFillColor(back->Normal);
    back->setPosition(sf::Vector2f(Window.getSize().y/2-back->Shape.getSize().y/2,450));
    back->setClickCallBack(GotoMainMenu);
    back->setMouseInCallBack(PlayOnHover);

    FinalMessage.setFont(fGentium);
    FinalMessage.setColor(sf::Color(65,65,65));
    FinalMessage.setPosition(20,25);
    FinalMessage.setCharacterSize(30);
    FinalMessage.setStyle(sf::Text::Bold);
}
void STATE::TEST_END::destroy() {
    cGUIManager::getInstance()->destroyAllElements();
    delete instance;
    instance = NULL;
}
void STATE::TEST_END::GotoMainMenu() {
    cSoundManager::getInstance()->Player.setVolume(100);
    cSoundManager::getInstance()->play(FILEPATH::SOUND::CLICK_4);
    cStateManager::getInstance()->setState(STATE::MAIN_MENU::INSTANCE());
}
// should be playOnHover, but this is a little hack to get past the multiple definition errors
void STATE::TEST_END::PlayOnHover() {
    cSoundManager::getInstance()->Player.setVolume(25);
    cSoundManager::getInstance()->play(FILEPATH::SOUND::CLICK_3);
}

void STATE::TEST_END::setParameters(const float percentage,const int questionsFinished,const string timeTaken) {
    string buffer;
    stringstream perc;
    perc << percentage;
    stringstream questions1;
    questions1 << questionsFinished;
    buffer = "You finished " + questions1.str() + " questions with an " + perc.str() + " % average \nwith a time of " + timeTaken + " seconds.\n\nThanks for playing!";
    FinalMessage.setString(buffer);
}
