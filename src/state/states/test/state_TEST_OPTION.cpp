#include "state_TEST_OPTION.h"
#include "../../../gui/cGUIButton.h"
#include "../../../gui/cGUICheckbox.h"
// TEST_OPTION
STATE::TEST_OPTION *STATE::TEST_OPTION::instance = NULL;
STATE::TEST_OPTION *STATE::TEST_OPTION::INSTANCE() {
    if(instance == NULL) {
        instance = new TEST_OPTION;
    }
    return instance;
}
sf::Texture STATE::TEST_OPTION::tMenuCheckboxBlank;
sf::Texture STATE::TEST_OPTION::tMenuCheckboxMarked;
// create subject buttons
/**
    1-Macbeth
    2-LOTF
    3-Terms
    4-After you my dear Alphonse
    5-Brother Death
    6-Butcher Bird
    7-Secret Life of Walter mitty
    8-Doll's house
    9-The fly
    10-The giraffe
**/
void STATE::TEST_OPTION::create() {
    // load the background
    if(!tBackground.loadFromFile(FILEPATH::TEXTURE::BACKGROUND_TESTOPTION)) {
        // log it and exit
        cLogManager::getInstance()->log(ERR::NOLOAD::TEXTURE::BACKGROUND_TESTOPTION,LOG::DIR,LOG::FILE);
        cErrorManager::getInstance()->displayError(ERR::NOLOAD::TEXTURE::BACKGROUND_TESTOPTION,LOG::DIR,LOG::FILE);
    }
    sBackground.setTexture(tBackground);
    // load the button font
    if(!fGentium.loadFromFile(FILEPATH::FONT::GENTIUM)) {
        // log it and exit
        cLogManager::getInstance()->log(ERR::NOLOAD::FONT::GENTIUM,LOG::DIR,LOG::FILE);
        cErrorManager::getInstance()->displayError(ERR::NOLOAD::FONT::GENTIUM,LOG::DIR,LOG::FILE);
    }
    // load the button texture
    if(!tMenuButton.loadFromFile(FILEPATH::TEXTURE::MENUBUTTON)) {
        // log it and exit
        cLogManager::getInstance()->log(ERR::NOLOAD::TEXTURE::MENUBUTTON,LOG::DIR,LOG::FILE);
        cErrorManager::getInstance()->displayError(ERR::NOLOAD::TEXTURE::MENUBUTTON,LOG::DIR,LOG::FILE);
    }
    // load the checkbox textures
    if(!tMenuCheckboxBlank.loadFromFile(FILEPATH::TEXTURE::CHECKBOXBLANK)) {
        // log it and exit
        cLogManager::getInstance()->log(ERR::NOLOAD::TEXTURE::CHECKBOXBLANK,LOG::DIR,LOG::FILE);
        cErrorManager::getInstance()->displayError(ERR::NOLOAD::TEXTURE::CHECKBOXBLANK,LOG::DIR,LOG::FILE);
    }
    if(!tMenuCheckboxMarked.loadFromFile(FILEPATH::TEXTURE::CHECKBOXMARKED)) {
        // log it and exit
        cLogManager::getInstance()->log(ERR::NOLOAD::TEXTURE::CHECKBOXMARKED,LOG::DIR,LOG::FILE);
        cErrorManager::getInstance()->displayError(ERR::NOLOAD::TEXTURE::CHECKBOXMARKED,LOG::DIR,LOG::FILE);
    }

    // init the buttons and checkboxes
    cGUIManager::getInstance()->createElement(new cGUICheckbox("checkbox1"));
    cGUICheckbox *checkbox1 = dynamic_cast<cGUICheckbox *>(cGUIManager::getInstance()->getElement("checkbox1"));
    checkbox1->setString("Macbeth");
    checkbox1->Text.setCharacterSize(25);
    checkbox1->Text.setColor(sf::Color(65,65,65));
    checkbox1->Shape.setTexture(&tMenuCheckboxBlank,true);
    checkbox1->Shape.setSize(sf::Vector2f(27,27));
    checkbox1->setPosition(sf::Vector2f(111,225));
    checkbox1->Text.setFont(fGentium);
    checkbox1->setMouseInCallBack(PlayOnHover);
    checkbox1->setClickCallBack(UpdateCheckboxTextures);

    cGUIManager::getInstance()->createElement(new cGUICheckbox("checkbox2"));
    cGUICheckbox *checkbox2 = dynamic_cast<cGUICheckbox *>(cGUIManager::getInstance()->getElement("checkbox2"));
    checkbox2->setString("Lord of the Flies");
    checkbox2->Text.setCharacterSize(25);
    checkbox2->Text.setColor(sf::Color(65,65,65));
    checkbox2->Shape.setTexture(&tMenuCheckboxBlank,true);
    checkbox2->Shape.setSize(sf::Vector2f(27,27));
    checkbox2->setPosition(sf::Vector2f(checkbox1->Shape.getPosition().x,
                                        checkbox1->Shape.getPosition().y+checkbox2->Shape.getLocalBounds().height));
    checkbox2->Text.setFont(fGentium);
    checkbox2->setMouseInCallBack(PlayOnHover);
    checkbox2->setClickCallBack(UpdateCheckboxTextures);

    cGUIManager::getInstance()->createElement(new cGUICheckbox("checkbox3"));
    cGUICheckbox *checkbox3 = dynamic_cast<cGUICheckbox *>(cGUIManager::getInstance()->getElement("checkbox3"));
    checkbox3->setString("Doll's house");
    checkbox3->Text.setCharacterSize(25);
    checkbox3->Text.setColor(sf::Color(65,65,65));
    checkbox3->Shape.setTexture(&tMenuCheckboxBlank,true);
    checkbox3->Shape.setSize(sf::Vector2f(27,27));
    checkbox3->setPosition(sf::Vector2f(checkbox2->Shape.getPosition().x,
                                        checkbox2->Shape.getPosition().y+checkbox3->Shape.getLocalBounds().height));
    checkbox3->Text.setFont(fGentium);
    checkbox3->setMouseInCallBack(PlayOnHover);
    checkbox3->setClickCallBack(UpdateCheckboxTextures);

    cGUIManager::getInstance()->createElement(new cGUICheckbox("checkbox4"));
    cGUICheckbox *checkbox4 = dynamic_cast<cGUICheckbox *>(cGUIManager::getInstance()->getElement("checkbox4"));
    checkbox4->setString("Poetry Terms");
    checkbox4->Text.setCharacterSize(25);
    checkbox4->Text.setColor(sf::Color(65,65,65));
    checkbox4->Shape.setTexture(&tMenuCheckboxBlank,true);
    checkbox4->Shape.setSize(sf::Vector2f(27,27));
    checkbox4->setPosition(sf::Vector2f(checkbox3->Shape.getPosition().x,
                                        checkbox3->Shape.getPosition().y+checkbox4->Shape.getLocalBounds().height));
    checkbox4->Text.setFont(fGentium);
    checkbox4->setMouseInCallBack(PlayOnHover);
    checkbox4->setClickCallBack(UpdateCheckboxTextures);

    cGUIManager::getInstance()->createElement(new cGUICheckbox("checkbox5"));
    cGUICheckbox *checkbox5 = dynamic_cast<cGUICheckbox *>(cGUIManager::getInstance()->getElement("checkbox5"));
    checkbox5->setString("The fly");
    checkbox5->Text.setCharacterSize(25);
    checkbox5->Text.setColor(sf::Color(65,65,65));
    checkbox5->Shape.setTexture(&tMenuCheckboxBlank,true);
    checkbox5->Shape.setSize(sf::Vector2f(27,27));
    checkbox5->setPosition(sf::Vector2f(checkbox4->Shape.getPosition().x,
                                        checkbox4->Shape.getPosition().y+checkbox5->Shape.getLocalBounds().height));
    checkbox5->Text.setFont(fGentium);
    checkbox5->setMouseInCallBack(PlayOnHover);
    checkbox5->setClickCallBack(UpdateCheckboxTextures);

    cGUIManager::getInstance()->createElement(new cGUICheckbox("checkbox6"));
    cGUICheckbox *checkbox6 = dynamic_cast<cGUICheckbox *>(cGUIManager::getInstance()->getElement("checkbox6"));
    checkbox6->setString("The giraffe");
    checkbox6->Text.setCharacterSize(25);
    checkbox6->Text.setColor(sf::Color(65,65,65));
    checkbox6->Shape.setTexture(&tMenuCheckboxBlank,true);
    checkbox6->Shape.setSize(sf::Vector2f(27,27));
    checkbox6->setPosition(sf::Vector2f(checkbox5->Shape.getPosition().x,
                                        checkbox5->Shape.getPosition().y+checkbox6->Shape.getLocalBounds().height));
    checkbox6->Text.setFont(fGentium);
    checkbox6->setMouseInCallBack(PlayOnHover);
    checkbox6->setClickCallBack(UpdateCheckboxTextures);

    cGUIManager::getInstance()->createElement(new cGUICheckbox("checkbox7"));
    cGUICheckbox *checkbox7 = dynamic_cast<cGUICheckbox *>(cGUIManager::getInstance()->getElement("checkbox7"));
    checkbox7->setString("After you my dear Alphonse");
    checkbox7->Text.setCharacterSize(25);
    checkbox7->Text.setColor(sf::Color(65,65,65));
    checkbox7->Shape.setTexture(&tMenuCheckboxBlank,true);
    checkbox7->Shape.setSize(sf::Vector2f(27,27));
    checkbox7->setPosition(sf::Vector2f(checkbox6->Shape.getPosition().x,
                                        checkbox6->Shape.getPosition().y+checkbox7->Shape.getLocalBounds().height));
    checkbox7->Text.setFont(fGentium);
    checkbox7->setMouseInCallBack(PlayOnHover);
    checkbox7->setClickCallBack(UpdateCheckboxTextures);

    cGUIManager::getInstance()->createElement(new cGUICheckbox("checkbox8"));
    cGUICheckbox *checkbox8 = dynamic_cast<cGUICheckbox *>(cGUIManager::getInstance()->getElement("checkbox8"));
    checkbox8->setString("Brother Death");
    checkbox8->Text.setCharacterSize(25);
    checkbox8->Text.setColor(sf::Color(65,65,65));
    checkbox8->Shape.setTexture(&tMenuCheckboxBlank,true);
    checkbox8->Shape.setSize(sf::Vector2f(27,27));
    checkbox8->setPosition(sf::Vector2f(checkbox7->Shape.getPosition().x,
                                        checkbox7->Shape.getPosition().y+checkbox8->Shape.getLocalBounds().height));
    checkbox8->Text.setFont(fGentium);
    checkbox8->setMouseInCallBack(PlayOnHover);
    checkbox8->setClickCallBack(UpdateCheckboxTextures);

    cGUIManager::getInstance()->createElement(new cGUICheckbox("checkbox9"));
    cGUICheckbox *checkbox9 = dynamic_cast<cGUICheckbox *>(cGUIManager::getInstance()->getElement("checkbox9"));
    checkbox9->setString("Butcher Bird");
    checkbox9->Text.setCharacterSize(25);
    checkbox9->Text.setColor(sf::Color(65,65,65));
    checkbox9->Shape.setTexture(&tMenuCheckboxBlank,true);
    checkbox9->Shape.setSize(sf::Vector2f(27,27));
    checkbox9->setPosition(sf::Vector2f(checkbox8->Shape.getPosition().x,
                                        checkbox8->Shape.getPosition().y+checkbox9->Shape.getLocalBounds().height));
    checkbox9->Text.setFont(fGentium);
    checkbox9->setMouseInCallBack(PlayOnHover);
    checkbox9->setClickCallBack(UpdateCheckboxTextures);

    cGUIManager::getInstance()->createElement(new cGUICheckbox("checkbox10"));
    cGUICheckbox *checkbox10 = dynamic_cast<cGUICheckbox *>(cGUIManager::getInstance()->getElement("checkbox10"));
    checkbox10->setString("Secret life of Walter Mitty");
    checkbox10->Text.setCharacterSize(25);
    checkbox10->Text.setColor(sf::Color(65,65,65));
    checkbox10->Shape.setTexture(&tMenuCheckboxBlank,true);
    checkbox10->Shape.setSize(sf::Vector2f(27,27));
    checkbox10->setPosition(sf::Vector2f(checkbox9->Shape.getPosition().x,
                                         checkbox9->Shape.getPosition().y+checkbox10->Shape.getLocalBounds().height));
    checkbox10->Text.setFont(fGentium);
    checkbox10->setMouseInCallBack(PlayOnHover);
    checkbox10->setClickCallBack(UpdateCheckboxTextures);

    // now create the options
    // timer
    // mark immediately
    // number of questions in total

    // start button
    cGUIManager::getInstance()->createElement(new cGUIButton("button1"));
    cGUIButton *button1 = dynamic_cast<cGUIButton *>(cGUIManager::getInstance()->getElement("button1"));
    button1->setString("Start");
    button1->Text.setFont(fGentium);
    button1->Text.setColor(sf::Color(65,65,65));
    button1->Shape.setTexture(&tMenuButton,true);
    button1->Normal = sf::Color(240,240,240);
    button1->Tinted = sf::Color(255,255,255);
    button1->Shape.setFillColor(button1->Normal);
    button1->setPosition(sf::Vector2f(525,510));
    button1->setClickCallBack(GotoTest);
    button1->setMouseInCallBack(PlayOnHover);

    // back button
    cGUIManager::getInstance()->createElement(new cGUIButton("button2"));
    cGUIButton *button2 = dynamic_cast<cGUIButton *>(cGUIManager::getInstance()->getElement("button2"));
    button2->setString("Back");
    button2->Text.setFont(fGentium);
    button2->Text.setColor(sf::Color(65,65,65));
    button2->Shape.setTexture(&tMenuButton,true);
    button2->Normal = sf::Color(240,240,240);
    button2->Tinted = sf::Color(255,255,255);
    button2->Shape.setFillColor(button2->Normal);
    button2->setPosition(sf::Vector2f(25,510));
    button2->setClickCallBack(GotoMainMenu);
    button2->setMouseInCallBack(PlayOnHover);
}
void STATE::TEST_OPTION::destroy() {
    cGUIManager::getInstance()->destroyAllElements();
    delete instance;
    instance = NULL;
}

void STATE::TEST_OPTION::update() {
    cGUIManager::getInstance()->updateElements();
}
void STATE::TEST_OPTION::render() {
    Window.clear();
    Window.draw(sBackground);
    cGUIManager::getInstance()->renderElements();
    Window.display();
}

void STATE::TEST_OPTION::UpdateCheckboxTextures() {
    for(unsigned int x=1; x<11; x++) {
        stringstream ss;
        ss << x;
        cGUICheckbox *checkbox = dynamic_cast<cGUICheckbox *>(cGUIManager::getInstance()->getElement("checkbox" + ss.str()));
        if(checkbox->isSelected) {
            // change the texture
            checkbox->Shape.setTexture(&tMenuCheckboxMarked,true);
        } else {
            // change the texture
            checkbox->Shape.setTexture(&tMenuCheckboxBlank,true);
        }
    }
}

void STATE::TEST_OPTION::GotoMainMenu() {
    cSoundManager::getInstance()->Player.setVolume(100);
    cSoundManager::getInstance()->play(FILEPATH::SOUND::CLICK_4);
    cStateManager::getInstance()->setState(STATE::MAIN_MENU::INSTANCE());
}
// should be playOnHover, but this is a little hack to get past the multiple definition errors
void STATE::TEST_OPTION::PlayOnHover() {
    cSoundManager::getInstance()->Player.setVolume(35);
    cSoundManager::getInstance()->play(FILEPATH::SOUND::CLICK_3);
}
void STATE::TEST_OPTION::GotoTest() {
    vector<bool> checkboxOptions;
    checkboxOptions.resize(10);
    bool willChange = false;
    for(unsigned int x=1; x<11; x++) {
        stringstream ss;
        ss << x;
        cGUICheckbox *checkbox = dynamic_cast<cGUICheckbox *>(cGUIManager::getInstance()->getElement("checkbox" + ss.str()));
        checkboxOptions[x-1] = checkbox->isSelected;
        if(checkbox->isSelected) {
            willChange = true;
        }
    }

    if(willChange) {
        cSoundManager::getInstance()->Player.setVolume(100);
        cSoundManager::getInstance()->play(FILEPATH::SOUND::CLICK_4);
        STATE::TEST::INSTANCE()->setTestParameters(checkboxOptions[0],
                checkboxOptions[1],
                checkboxOptions[2],
                checkboxOptions[3],
                checkboxOptions[4],
                checkboxOptions[5],
                checkboxOptions[6],
                checkboxOptions[7],
                checkboxOptions[8],
                checkboxOptions[9]);
        cStateManager::getInstance()->setState(STATE::TEST::INSTANCE());
    } else {
        cSoundManager::getInstance()->Player.setVolume(50);
        cSoundManager::getInstance()->play(FILEPATH::SOUND::ERROR_1);
    }
}

