#ifndef CTEST_H
#define CTEST_H

#include "../../../../globals/globals.h"

int randInt(int,int);

enum SUBJECT {
    MACBETH = 0,
    LOTF = 1,
    DOLLS_HOUSE = 2,
    TERMS = 3,
    THE_FLY = 4,
    THE_GIRAFFE = 5,
    AFTER_YOU = 6,
    BROTHER_DEATH = 7,
    BUTCHER_BIRD = 8,
    SECRET_LIFE = 9
};

enum NUMBEROFQUESTIONS {
    NMACBETH = 15,
    NLOTF = 15,
    NDOLLS_HOUSE = 10,
    NTERMS = 15,
    NTHE_FLY = 10,
    NTHE_GIRAFFE = 10,
    NAFTER_YOU = 10,
    NBROTHER_DEATH = 10,
    NBUTCHER_BIRD = 10,
    NSECRET_LIFE = 10
};

class cQMultipleChoice {
public:
    string Question;
    string AnswerA; // this is the correct answer
    string AnswerB;
    string AnswerC;
    string AnswerD;
};

/**
    The list is ordered as such:
        - Macbeth
        - LOTF
        - Dolls house
        - Terms
        - The fly
        - The giraffe
        - After you
        - Brother death
        - butcher bird
        - secret life
**/

class cTest {
private:
    vector<cQMultipleChoice> Question;
public:
    cTest();    // this is where all of the questions are initialized
    cQMultipleChoice * CurrentQuestion;
    cQMultipleChoice * getRandomQuestion(const int subject);
};

#endif // CTEST_H
