#include "cTest.h"

cTest::cTest() {
    /** MACBETH 15 TOTAL **/
    cQMultipleChoice bufferQuestion;
    bufferQuestion.Question = "Who kills Macbeth?";
    bufferQuestion.AnswerA = "Macduff";
    bufferQuestion.AnswerB = "Banquo";
    bufferQuestion.AnswerC = "Lady Macbeth";
    bufferQuestion.AnswerD = "Malcolm";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "Who do Macbeth and Lady Macbeth frame for the murder of Duncan?";
    bufferQuestion.AnswerA = "Duncan�s drunken chamberlains";
    bufferQuestion.AnswerB = "Malcolm and Donalbain";
    bufferQuestion.AnswerC = "The drunk porter";
    bufferQuestion.AnswerD = "Fleance";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "Who discovers Duncan�s body?";
    bufferQuestion.AnswerA = "Macduff";
    bufferQuestion.AnswerB = "Lennox";
    bufferQuestion.AnswerC = "Ross";
    bufferQuestion.AnswerD = "Donalbain";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "Who does Macbeth see sitting in his chair during the banquet?";
    bufferQuestion.AnswerA = "Banquo�s ghost";
    bufferQuestion.AnswerB = "Duncan�s ghost";
    bufferQuestion.AnswerC = "Donalbain's ghost";
    bufferQuestion.AnswerD = "Lady Macbeth";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "What vision does Macbeth have before he kills Duncan?";
    bufferQuestion.AnswerA = "a floating dagger pointing him to Duncan�s chamber";
    bufferQuestion.AnswerB = "a floating head urging him to spill blood";
    bufferQuestion.AnswerC = "a bloody axe lodged in Duncan�s brow";
    bufferQuestion.AnswerD = "a bloody axe lodged in Banquo's head";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "With whom are the Scots at war at the beginning of the play?";
    bufferQuestion.AnswerA = "Norway";
    bufferQuestion.AnswerB = "Denmark";
    bufferQuestion.AnswerC = "Canada";
    bufferQuestion.AnswerD = "Finland";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "What happens to Lady Macbeth before she dies?";
    bufferQuestion.AnswerA = "She is plagued by fits of sleepwalking";
    bufferQuestion.AnswerB = "She is haunted by the ghost of Duncan";
    bufferQuestion.AnswerC = "She sees her children killed in battle";
    bufferQuestion.AnswerD = "She sees her children killed by Macbeth";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "Why is Macduff able to kill Macbeth despite the witches� prophecy?";
    bufferQuestion.AnswerA = "He was born by cesarean section";
    bufferQuestion.AnswerB = "He kills the witches first";
    bufferQuestion.AnswerC = "He receives a charm from Grinswindle";
    bufferQuestion.AnswerD = "He is a witch himself";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "Who flees Scotland to join Malcolm in England?";
    bufferQuestion.AnswerA = "Macduff";
    bufferQuestion.AnswerB = "Donalbain";
    bufferQuestion.AnswerC = "Fleance";
    bufferQuestion.AnswerD = "Lennox";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "Who flees Scotland immediately after Duncan�s death?";
    bufferQuestion.AnswerA = "Malcolm and Donalbain";
    bufferQuestion.AnswerB = "Fleance";
    bufferQuestion.AnswerC = "Macbeth and Ross";
    bufferQuestion.AnswerD = "Lennox";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "The witches greet Macbeth as ?";
    bufferQuestion.AnswerA = "Thane of Cawdor and Thane of Fife and King hereafter";
    bufferQuestion.AnswerB = "Thane of Glamis and Thane of Cawdor and King hereafter";
    bufferQuestion.AnswerC = "Thane of Glamis and Thane of Fife and father of kings";
    bufferQuestion.AnswerD = "Thane of Cawdor and Thane of Lochaber and Thane of Glamis";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "How does Birnam Wood come to Dunsinane?";
    bufferQuestion.AnswerA = "Malcolm's army disguise themselves as trees by cutting off the branches";
    bufferQuestion.AnswerB = "The witches cast a spell and make the forest move to the castle";
    bufferQuestion.AnswerC = "The forest comes alive and kills Macbeth";
    bufferQuestion.AnswerD = "It doesn't- its a forest";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "Lady Macbeth calls on spirits to ?";
    bufferQuestion.AnswerA = "Unsex her";
    bufferQuestion.AnswerB = "kill Banquo";
    bufferQuestion.AnswerC = "Become queen";
    bufferQuestion.AnswerD = "Kill Fleance";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "Why couldn't Lady Macbeth kill Duncan?";
    bufferQuestion.AnswerA = "He resembled her father";
    bufferQuestion.AnswerB = "He is her father-in-law";
    bufferQuestion.AnswerC = "He looks like her dear brother";
    bufferQuestion.AnswerD = "She would have woken him";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "In Act IV, the witches tell Macbeth he cannot be killed ?";
    bufferQuestion.AnswerA = "until Birnam Wood comes to Dunsinane";
    bufferQuestion.AnswerB = "until Banquo is king";
    bufferQuestion.AnswerC = "until Lady Macbeth is killed";
    bufferQuestion.AnswerD = "until Fleance rules";
    Question.push_back(bufferQuestion);

    /** LOTF 15 TOTAL **/
    bufferQuestion.Question = "What do the boys have that is the symbol of authority in the society they form? ";
    bufferQuestion.AnswerA = "Conch shell";
    bufferQuestion.AnswerB = "Whale jaw bone";
    bufferQuestion.AnswerC = "British flag";
    bufferQuestion.AnswerD = "Piggy's glasses";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "Who are the hunters, and what is their job?";
    bufferQuestion.AnswerA = "The choirboys; getting food";
    bufferQuestion.AnswerB = "The littluns; looking for ships and planes";
    bufferQuestion.AnswerC = "Samneric; killing dangerous animals";
    bufferQuestion.AnswerD = "Simon, Piggy, Ralph; killing pigs";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "What causes the hunters to neglect the signal fire and allow it to go out?";
    bufferQuestion.AnswerA = "They can only think about hunting";
    bufferQuestion.AnswerB = "They become more interested in escaping the island, so instead\n they try to build a canoe";
    bufferQuestion.AnswerC = "They try to rebel against Ralph's orders";
    bufferQuestion.AnswerD = "They are inexperienced and don't really know how to keep it burning";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "What is Simon saying when he thinks the 'beast' may be inside\n the boys themselves?";
    bufferQuestion.AnswerA = "The dark side of the human personality can destroy mankind.";
    bufferQuestion.AnswerB = "They have eaten some poisoned meat";
    bufferQuestion.AnswerC = "He thinks that none of them are really human, as he has gone mad";
    bufferQuestion.AnswerD = "It is all in their imaginations";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "What is the meaning of the Lord of the Flies' message to Simon?";
    bufferQuestion.AnswerA = "Evil is a trait inside of man";
    bufferQuestion.AnswerB = "Dreams can be more powerful than reality";
    bufferQuestion.AnswerC = "It is unhealthy to eat uncooked meat";
    bufferQuestion.AnswerD = "He will soon die from the lack of fresh water";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "What happens to Simon when he returns to the group?";
    bufferQuestion.AnswerA = "The hunters kill him before he can tell them anything";
    bufferQuestion.AnswerB = "He keeps quiet because he knows they won't believe him";
    bufferQuestion.AnswerC = "He joins in the dance and becomes evil himself";
    bufferQuestion.AnswerD = "He tells Ralph the truth, and then goes off to live alone";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "How is Ralph saved in the end?";
    bufferQuestion.AnswerA = "A British naval officer finds him";
    bufferQuestion.AnswerB = "The littluns push Jack off the mountain top into the sea";
    bufferQuestion.AnswerC = "He is stronger than Jack and defeats him";
    bufferQuestion.AnswerD = "He swims to the safety of another island";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "Who is the first boy to disappear?";
    bufferQuestion.AnswerA = "A littlun";
    bufferQuestion.AnswerB = "Maurice";
    bufferQuestion.AnswerC = "Simon";
    bufferQuestion.AnswerD = "Jack";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "On an allegorical level, The Lord of the Flies is not only a\n book about a bunch of young boys on an island, but also a book about ?";
    bufferQuestion.AnswerA = "Savage side of Man";
    bufferQuestion.AnswerB = "Mankind�s inherent kindness";
    bufferQuestion.AnswerC = "Human nature";
    bufferQuestion.AnswerD = "Hunting";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "Who kills Simon?";
    bufferQuestion.AnswerA = "Everyone";
    bufferQuestion.AnswerB = "Jack";
    bufferQuestion.AnswerC = "Piggy";
    bufferQuestion.AnswerD = "Maurice";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "In the presence of the naval officer, the boys were ?";
    bufferQuestion.AnswerA = "were relieved, exhausted, and upset by all that had happened";
    bufferQuestion.AnswerB = "belligerent towards the officer, trying continually to distract \nhim from Ralph";
    bufferQuestion.AnswerC = "still savage yet frightened by the sharply dressed officer";
    bufferQuestion.AnswerD = "caught completely offguard, and ran back into the forest";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "Which of these does not take place when Ralph�s group arrives at Castle Rock ?";
    bufferQuestion.AnswerA = "Jack is killed";
    bufferQuestion.AnswerB = "A fight breaks out";
    bufferQuestion.AnswerC = "Piggy is killed";
    bufferQuestion.AnswerD = "The twins are captured";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "For Jack�s tribe, the need for meat becomes secondary to the ?";
    bufferQuestion.AnswerA = "Lust to kill";
    bufferQuestion.AnswerB = "Need for security and order";
    bufferQuestion.AnswerC = "Need for a new clan to overthrow Ralph";
    bufferQuestion.AnswerD = "Need for shelter and water ";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "When does the first boy disappear?";
    bufferQuestion.AnswerA = "After the boy's signal fire gets out of control";
    bufferQuestion.AnswerB = "After a boar eats Simon";
    bufferQuestion.AnswerC = "After the naval officer arrives";
    bufferQuestion.AnswerD = "After Jack decides to ambush Ralph and Piggy";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "Who tells Jack where Ralph is hiding in Chapter 12?";
    bufferQuestion.AnswerA = "Samneric";
    bufferQuestion.AnswerB = "Simon";
    bufferQuestion.AnswerC = "Maurice";
    bufferQuestion.AnswerD = "A littlun";
    Question.push_back(bufferQuestion);

    /** DOLLS HOUSE  9 TOTAL **/
    bufferQuestion.Question = "The Kelveys were known to wear ?";
    bufferQuestion.AnswerA = "Bits of cloth given to their mother";
    bufferQuestion.AnswerB = "Overly expensive clothes";
    bufferQuestion.AnswerC = "Scraps of burlap from their farm";
    bufferQuestion.AnswerD = "Plaid garments in the colours of their clan";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "The reason given by Mrs. Burnell regarding their treatment of the Kelveys is:";
    bufferQuestion.AnswerA = "None";
    bufferQuestion.AnswerB = "They are poorer and interaction would lower their social standing.";
    bufferQuestion.AnswerC = "They are foreign and interaction would lower their social standing.";
    bufferQuestion.AnswerD = "They are poorer and therefore deserve sympathy.";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "Kezia believes that the __ is the best part of the doll's house";
    bufferQuestion.AnswerA = "Lamp";
    bufferQuestion.AnswerB = "Smell";
    bufferQuestion.AnswerC = "Dolls";
    bufferQuestion.AnswerD = "Wallpaper";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "Lena Logan is pushed to insult __?";
    bufferQuestion.AnswerA = "Lil Kelvey";
    bufferQuestion.AnswerB = "Kezia Burnell";
    bufferQuestion.AnswerC = "Mrs. Hay";
    bufferQuestion.AnswerD = "The Judge";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "The teacher had a special smile for:";
    bufferQuestion.AnswerA = "when Lil brings her flowers";
    bufferQuestion.AnswerB = "Pat when he does the rounds.";
    bufferQuestion.AnswerC = "The Burnells.";
    bufferQuestion.AnswerD = "When usual smile just doesn't cut it.";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "The doll's house is NOT described as having which of the following?";
    bufferQuestion.AnswerA = "A stove with real twigs";
    bufferQuestion.AnswerB = "Beds with real bedclothes.";
    bufferQuestion.AnswerC = "A family of four dolls.";
    bufferQuestion.AnswerD = "A lamp in the dining room.";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "Kezia invites the Kelveys over for what purpose?";
    bufferQuestion.AnswerA = "To show them the doll's house";
    bufferQuestion.AnswerB = "To spite her parents and aunt.";
    bufferQuestion.AnswerC = "To humiliate them for having thought they actually \nhad friends.";
    bufferQuestion.AnswerD = "To show her parents that people are people no matter \nhow poor.";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "Which of the following are simblings?";
    bufferQuestion.AnswerA = "Isabel, Lottie, Kezia ";
    bufferQuestion.AnswerB = "Aunt Beryl, Pat";
    bufferQuestion.AnswerC = "Lil, Else, Tod";
    bufferQuestion.AnswerD = "Lena, Emmie";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "Who didn't go prepare for visitors to his/her house?";
    bufferQuestion.AnswerA = "Kezia";
    bufferQuestion.AnswerB = "Jessie May";
    bufferQuestion.AnswerC = "Isabel";
    bufferQuestion.AnswerD = "Else";
    Question.push_back(bufferQuestion);

    /** TERMS  **/
    bufferQuestion.Question = "An example of a metaphor is:";
    bufferQuestion.AnswerA = "The moon was a ghostly galleon.";
    bufferQuestion.AnswerB = "The sun sank below the southern shore.";
    bufferQuestion.AnswerC = "Away to the window I flew like a flash.";
    bufferQuestion.AnswerD = "I doubt if Phaethon feared more.";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "'Back he spurred like a madman' is an example of:";
    bufferQuestion.AnswerA = "Simile";
    bufferQuestion.AnswerB = "Alliteration";
    bufferQuestion.AnswerC = "Metaphor";
    bufferQuestion.AnswerD = "Internal rhyme";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "'Yet he wields the touch of Midas'' is an example of:";
    bufferQuestion.AnswerA = "Allusion";
    bufferQuestion.AnswerB = "Imagery";
    bufferQuestion.AnswerC = "Illusion";
    bufferQuestion.AnswerD = "Consonance";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "'The good king, as you say, shall rue the day' is an example of:";
    bufferQuestion.AnswerA = "Internal rhyme";
    bufferQuestion.AnswerB = "Allusion";
    bufferQuestion.AnswerC = "Symbolism";
    bufferQuestion.AnswerD = "Homophone";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "'Elementary, my dear Watson' is an example of:";
    bufferQuestion.AnswerA = "Allusion";
    bufferQuestion.AnswerB = "Assonance";
    bufferQuestion.AnswerC = "Juxtaposition";
    bufferQuestion.AnswerD = "Euphemism";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "A dilemma is:";
    bufferQuestion.AnswerA = "A choice with no desirable outcomes.";
    bufferQuestion.AnswerB = "A contradictory statement";
    bufferQuestion.AnswerC = "An unsolvable riddle";
    bufferQuestion.AnswerD = "An unfortunate series of events";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "Antecedent action is:";
    bufferQuestion.AnswerA = "Events prior to a story's beginning.";
    bufferQuestion.AnswerB = "A preemptive response to a predicted event";
    bufferQuestion.AnswerC = "That which occurs before the climax";
    bufferQuestion.AnswerD = "That which occurs after the climax";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "The inciting event that leads to rising action is known as the:";
    bufferQuestion.AnswerA = "Initial incident";
    bufferQuestion.AnswerB = "Initial crisis";
    bufferQuestion.AnswerC = "Antagonistic incident";
    bufferQuestion.AnswerD = "Antagonistic crisis";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "A character displaying many different qualities is called:";
    bufferQuestion.AnswerA = "Round";
    bufferQuestion.AnswerB = "Polyhedral";
    bufferQuestion.AnswerC = "Ideal";
    bufferQuestion.AnswerD = "Dynamic";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "The basic rhythmic structure of a verse or line is known as the:";
    bufferQuestion.AnswerA = "Metre";
    bufferQuestion.AnswerB = "Rhyme scheme";
    bufferQuestion.AnswerC = "Iamb";
    bufferQuestion.AnswerD = "Foot";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "A fictional story featuring a moral is called:";
    bufferQuestion.AnswerA = "A fable";
    bufferQuestion.AnswerB = "A tale";
    bufferQuestion.AnswerC = "A myth";
    bufferQuestion.AnswerD = "Prose";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "A short story usually comprises of:";
    bufferQuestion.AnswerA = "A single mood";
    bufferQuestion.AnswerB = "Fewer chapters than a novel";
    bufferQuestion.AnswerC = "Many developing characters";
    bufferQuestion.AnswerD = "Short epilogue";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "The climax of a story can be defined as:";
    bufferQuestion.AnswerA = "The moment of highest intensity";
    bufferQuestion.AnswerB = "The prevailing of the protagonist over the antagonist";
    bufferQuestion.AnswerC = "The vanquishing of evil";
    bufferQuestion.AnswerD = "The moment of greatest relief";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "Recurring ideas or images are known as:";
    bufferQuestion.AnswerA = "Motifs";
    bufferQuestion.AnswerB = "Emphases";
    bufferQuestion.AnswerC = "Subliminal stimuli";
    bufferQuestion.AnswerD = "Symbols";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "Two characters with opposing qualities are called:";
    bufferQuestion.AnswerA = "Character Foils";
    bufferQuestion.AnswerB = "Character Opposites";
    bufferQuestion.AnswerC = "Complementary characters";
    bufferQuestion.AnswerD = "Static characters";
    Question.push_back(bufferQuestion);

    /** FLY 10 TOTAL **/
    bufferQuestion.Question = "For what purpose did Woodifield come to the boss?";
    bufferQuestion.AnswerA = "A relief from the restrictions of his wife.";
    bufferQuestion.AnswerB = "To reminisce about the good ol' days";
    bufferQuestion.AnswerC = "To help install the boss' new electric heating";
    bufferQuestion.AnswerD = "To pick up his share of the loot";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "Where did Mr. Woodifield's girls visit recently?";
    bufferQuestion.AnswerA = "Belgium";
    bufferQuestion.AnswerB = "The local pub";
    bufferQuestion.AnswerC = "The boss' son's house";
    bufferQuestion.AnswerD = "The wreckage of a medieval treasure ship";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "With whom does the boss share his whiskey?";
    bufferQuestion.AnswerA = "Mr. Woodifield";
    bufferQuestion.AnswerB = "Mrs. Woodifield";
    bufferQuestion.AnswerC = "Macey";
    bufferQuestion.AnswerD = "Reggie";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "From whence does the boss' whiskey supposedly come?";
    bufferQuestion.AnswerA = "Windsor Castle";
    bufferQuestion.AnswerB = "Belgium";
    bufferQuestion.AnswerC = "An ancient Venetian brewery";
    bufferQuestion.AnswerD = "The belongings of his son";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "What ended Woodifield's career in golf?";
    bufferQuestion.AnswerA = "never mentioned that he was a golfer";
    bufferQuestion.AnswerB = "An affair";
    bufferQuestion.AnswerC = "A stroke";
    bufferQuestion.AnswerD = "An unconstitutional witch hunt regarding an \nallegation of doping";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "The boss' son died:";
    bufferQuestion.AnswerA = "6 years prior";
    bufferQuestion.AnswerB = "In a car accident with Woodifield's son";
    bufferQuestion.AnswerC = "Of polio when he was but a wee lad";
    bufferQuestion.AnswerD = "None of the above; he's still alive";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "When the boss discovers a fly in his inkpot, he: ";
    bufferQuestion.AnswerA = "Uses his pen to flick it out";
    bufferQuestion.AnswerB = "Uses his pen to drown it";
    bufferQuestion.AnswerC = "Sends for Macey to deal with it";
    bufferQuestion.AnswerD = "Begins crying as he is reminded of a time his son had \nfallen into an inkpot";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "The boss' reaction to Woodifield mentioning a certain grave is one of:";
    bufferQuestion.AnswerA = "shock";
    bufferQuestion.AnswerB = "Indifference";
    bufferQuestion.AnswerC = "Interest";
    bufferQuestion.AnswerD = "Nostalgia";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "After the fly escaped from the pot, the boss was intrigued by:";
    bufferQuestion.AnswerA = "its perseverance in cleaning itself off";
    bufferQuestion.AnswerB = "Its immediate recuperation";
    bufferQuestion.AnswerC = "Its use of its wings to clean itself off";
    bufferQuestion.AnswerD = "How it seemed to have given and laid still";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "Mr. Woodifield lost someone close to him. Who?";
    bufferQuestion.AnswerA = "Reggie, his son";
    bufferQuestion.AnswerB = "His brother, the captain";
    bufferQuestion.AnswerC = "His youngest daughter, Mary";
    bufferQuestion.AnswerD = "His first wife";
    Question.push_back(bufferQuestion);

    /** The giraffe - 10 total **/
    bufferQuestion.Question = "The priest's first reaction to the giraffe was one of:";
    bufferQuestion.AnswerA = "Mild Entertainment";
    bufferQuestion.AnswerB = "Overwhelming joy";
    bufferQuestion.AnswerC = "Horror";
    bufferQuestion.AnswerD = "Incredulity";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "The fate of the giraffe's original owner was:";
    bufferQuestion.AnswerA = "To fall over dead of unknown causes.";
    bufferQuestion.AnswerB = "That of a traitor - hanged, drawn, and quartered.";
    bufferQuestion.AnswerC = "Being run out of town.";
    bufferQuestion.AnswerD = "To be elected as mayor.";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "Who decides to take ownership of the giraffe?";
    bufferQuestion.AnswerA = "Rolandino";
    bufferQuestion.AnswerB = "The policeman";
    bufferQuestion.AnswerC = "The priest";
    bufferQuestion.AnswerD = "The mayor";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "What is suspected to be the cause of the giraffe's death?";
    bufferQuestion.AnswerA = "It ate bad leaves";
    bufferQuestion.AnswerB = "The city hired a hunter to deal with it \nin secret.";
    bufferQuestion.AnswerC = "It suffered a seizure.";
    bufferQuestion.AnswerD = "It was hit by the policeman's car.";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "The owner(s) of the giraffe decided to keep it where?";
    bufferQuestion.AnswerA = "The church";
    bufferQuestion.AnswerB = "The public park.";
    bufferQuestion.AnswerC = "In Rolandino's barn.";
    bufferQuestion.AnswerD = "In the abandoned basketball court.";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "What does the policeman threaten to do to the giraffe?";
    bufferQuestion.AnswerA = "Kill it with his revolver.";
    bufferQuestion.AnswerB = "Have it butchered and cooked for the city's \nChristmas dinner.";
    bufferQuestion.AnswerC = "Have it shipped back to Africa.";
    bufferQuestion.AnswerD = "Take it away to the zoo.";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "What earned cries of fear and shame from the town's residents?";
    bufferQuestion.AnswerA = "The giraffe looking in their upstairs windows.";
    bufferQuestion.AnswerB = "The realization that they had caused the death of the giraffe.";
    bufferQuestion.AnswerC = "The policeman's threat towards the giraffe.";
    bufferQuestion.AnswerD = "The giraffe knocking over the statue of Jesus in the church.";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "The gathering in the town square was regarding what?";
    bufferQuestion.AnswerA = "The decision to kill the giraffe.";
    bufferQuestion.AnswerB = "The decision to build a zoo.";
    bufferQuestion.AnswerC = "The election of a new mayor.";
    bufferQuestion.AnswerD = "A protest regarding the mistreatment of the giraffe.";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "Which of the following sentences best conveys the message of the short story?";
    bufferQuestion.AnswerA = "There must have been other reasons too for the hatred\n of the people: like the defense of an equilibrium";
    bufferQuestion.AnswerB = "The people standing around said, 'Poor animal, what\n will we do with it?";
    bufferQuestion.AnswerC = "We thought we saw him smile, even though he charged\n us to take the beast away.";
    bufferQuestion.AnswerD = "Not only the children fell under a spell watching\n it, but we boys too";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "After being driven out, the giraffe's owner(s):";
    bufferQuestion.AnswerA = "Made plans to get the giraffe into the town, and ways\n of forcing it on the Mayor and the people.";
    bufferQuestion.AnswerB = "Made plans to return the giraffe back to its family \nin Africa.";
    bufferQuestion.AnswerC = "Decide(s) to live as the previous owner did: travelling\n from place to place, selling their wares.";
    bufferQuestion.AnswerD = "Decide(s) to kill the giraffe kindly and restore\n peace in the town.";
    Question.push_back(bufferQuestion);

    /** AfterYou - 10 TOTAL**/
    bufferQuestion.Question = "Who are the characters in 'After you my dear Alphonse'?";
    bufferQuestion.AnswerA = "Mrs. Wilson,her son Johnny and his friend Boyd";
    bufferQuestion.AnswerB = "Mrs.Wilson,her son Boyd and his friend Alphonse";
    bufferQuestion.AnswerC = "Mrs.Willow,her son Johnny and his friend Alphonse";
    bufferQuestion.AnswerD = "Mrs.Willow,her son Boyd and his friend Johnny";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "After Johnny's mother sees Boyd carrying wood, she ?";
    bufferQuestion.AnswerA = "Tells Boyd that Johnny should help him";
    bufferQuestion.AnswerB = "Becomes angry at Johnny for having a 'different' friend";
    bufferQuestion.AnswerC = "Does not allow Boyd to eat lunch";
    bufferQuestion.AnswerD = "Gives Boyd a small potato for his work";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "While dishing out lunch, Johnny's mother assumes Boyd ?";
    bufferQuestion.AnswerA = "Will eat any food";
    bufferQuestion.AnswerB = "Does not like tomatoes";
    bufferQuestion.AnswerC = "Likes potatoes nor tomatoes";
    bufferQuestion.AnswerD = "Does not like potatoes";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "While both boys are eating, Mrs.Wilson asks about Boyd's ?";
    bufferQuestion.AnswerA = "mother and father";
    bufferQuestion.AnswerB = "mother";
    bufferQuestion.AnswerC = "father";
    bufferQuestion.AnswerD = "potatoes";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "What does Mrs.Wilson assume about Boyd's father ?";
    bufferQuestion.AnswerA = "that he is a manual laborer";
    bufferQuestion.AnswerB = "that he is a foreman";
    bufferQuestion.AnswerC = "that he does not work";
    bufferQuestion.AnswerD = "that he is a potato and tomato farmer";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "What does Boyd's mother do ?";
    bufferQuestion.AnswerA = "she works as a stay at home mom";
    bufferQuestion.AnswerB = "she works at the same factory as her husband";
    bufferQuestion.AnswerC = "she works as a lawyer";
    bufferQuestion.AnswerD = "she used to work as a lawyer";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "What does Boyd's sister want to become ?";
    bufferQuestion.AnswerA = "A teacher";
    bufferQuestion.AnswerB = "A potato";
    bufferQuestion.AnswerC = "A nurse";
    bufferQuestion.AnswerD = "A lawyer";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "After hearing about Boyd's sister's aspirations,\n Mrs. Wilson has the urge to ?";
    bufferQuestion.AnswerA = "pat the boy on the head";
    bufferQuestion.AnswerB = "give him more tomatoes";
    bufferQuestion.AnswerC = "give him more potatoes";
    bufferQuestion.AnswerD = "give him a candy";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "After the boy's finish their meal, Mrs. Wilson ?";
    bufferQuestion.AnswerA = "Offers Boyd spare clothing";
    bufferQuestion.AnswerB = "Asks Boyd what he would like to be";
    bufferQuestion.AnswerC = "Offers them more tomatoes and potatoes";
    bufferQuestion.AnswerD = "Offers them more milk and cookies";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "Why was Mrs. Wilson dissapointed in Boyd ?";
    bufferQuestion.AnswerA = "He did not live up to her assumptions";
    bufferQuestion.AnswerB = "He did not finish his potatoes and tomatoes";
    bufferQuestion.AnswerC = "He did not have common courtesy to thank her \nfor his meal";
    bufferQuestion.AnswerD = "He had beaten her son in 'Tanks'";
    Question.push_back(bufferQuestion);

    /** BROTHER DEATH - 10  **/

    bufferQuestion.Question = "Ted and Mary's relationship can be best described as ?";
    bufferQuestion.AnswerA = "siblings";
    bufferQuestion.AnswerB = "cousins";
    bufferQuestion.AnswerC = "father and mother";
    bufferQuestion.AnswerD = "son and mother";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "What does Ted have ?";
    bufferQuestion.AnswerA = "A heart condition";
    bufferQuestion.AnswerB = "A lung disease";
    bufferQuestion.AnswerC = "polio";
    bufferQuestion.AnswerD = "cancer";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "How are the Aspinwahls and Grey's opposite ?";
    bufferQuestion.AnswerA = "Grey's have learned from experience while the\n Aspinwahl's are well educated";
    bufferQuestion.AnswerB = "Grey's are foolish with money and gamble";
    bufferQuestion.AnswerC = "Grey's are well educated while the Aspinwahl's \nhave learned from experience";
    bufferQuestion.AnswerD = "Aspinwahl's are foolish with money and gamble";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "What infuriates Ted the most ?";
    bufferQuestion.AnswerA = "Being treated as if he was 'special'";
    bufferQuestion.AnswerB = "his inability to walk ";
    bufferQuestion.AnswerC = "Being treated as 'weaker' and useless for farm-work";
    bufferQuestion.AnswerD = "when he is bullied for his inability to walk";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "Who is the eldest son of the Grey family ?";
    bufferQuestion.AnswerA = "Don";
    bufferQuestion.AnswerB = "Ted";
    bufferQuestion.AnswerC = "Henry";
    bufferQuestion.AnswerD = "John";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "What is the conflict between John and Louise ?";
    bufferQuestion.AnswerA = "Louise wants to keep trees planted by her Aspinwahl\n grandfather";
    bufferQuestion.AnswerB = "Louise wants to chop trees planted by John's Grey\n grandfather";
    bufferQuestion.AnswerC = "John wants to take down an old abandonned house \nmade by Louise's grandfather";
    bufferQuestion.AnswerD = "John wants to keep an old abandonned house made \nby Don";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "After the conflict, Don threatens to ___, but then ?";
    bufferQuestion.AnswerA = "Leave the farm,comes back";
    bufferQuestion.AnswerB = "chop the trees,Ted stops him";
    bufferQuestion.AnswerC = "chop the trees,Louise stops him";
    bufferQuestion.AnswerD = "chop the trees,is killed by John";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "The farm is ultimately passed onto ?";
    bufferQuestion.AnswerA = "Don";
    bufferQuestion.AnswerB = "Ted";
    bufferQuestion.AnswerC = "Henry";
    bufferQuestion.AnswerD = "No one in the time frame of the story";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "The death of Ted is compared with the death of ?";
    bufferQuestion.AnswerA = "Aspinwahl blood in Don";
    bufferQuestion.AnswerB = "Aspinwahl blood in Louise";
    bufferQuestion.AnswerC = "John";
    bufferQuestion.AnswerD = "Grey blood in John";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "Mary can be best described as ?";
    bufferQuestion.AnswerA = "intelligent and protective";
    bufferQuestion.AnswerB = "shy, awkward yet understanding";
    bufferQuestion.AnswerC = "shy, yet understanding";
    bufferQuestion.AnswerD = "shy and protective";
    Question.push_back(bufferQuestion);


    /** BUTHCER BIRD 10 TOTAL **/

    bufferQuestion.Question = "Who is the family going to visit at the start of the story ?";
    bufferQuestion.AnswerA = "The Garfields";
    bufferQuestion.AnswerB = "The Wilsons";
    bufferQuestion.AnswerC = "The Jamesons";
    bufferQuestion.AnswerD = "The Greys";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "The Garfields can be best described as ?";
    bufferQuestion.AnswerA = "comfortable, yet kind and well spoken ";
    bufferQuestion.AnswerB = "poor, yet soft and kind";
    bufferQuestion.AnswerC = "poor, yet well spoken and upright";
    bufferQuestion.AnswerD = "comfortable, yet grumpy";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "The Garfields are ?";
    bufferQuestion.AnswerA = "British";
    bufferQuestion.AnswerB = "Scottish";
    bufferQuestion.AnswerC = "American, but originally came from Scotland";
    bufferQuestion.AnswerD = "American";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "The boy's father mocks Mr. Garfield since ?";
    bufferQuestion.AnswerA = "Mr.Garfield is everything that he is not ";
    bufferQuestion.AnswerB = "Mr. Garfield did not care to ask his permission to \ngive his son a gun";
    bufferQuestion.AnswerC = "He is not as rich as Mr. Garfield";
    bufferQuestion.AnswerD = "He want Mr. Garfield to give him a gun";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "What does Mr. Garfield say not to shoot ?";
    bufferQuestion.AnswerA = "any animal that isn't bloodthirsty";
    bufferQuestion.AnswerB = "rodents";
    bufferQuestion.AnswerC = "rabbits";
    bufferQuestion.AnswerD = "any animal that doesn't kill livestock";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "What does Mr. Garfield give the boy ?";
    bufferQuestion.AnswerA = "A gun";
    bufferQuestion.AnswerB = "A gramophone";
    bufferQuestion.AnswerC = "A book";
    bufferQuestion.AnswerD = "Several seeds for gardening";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "The boy's mother asks Mr. Garfield to see his ?";
    bufferQuestion.AnswerA = "Trees";
    bufferQuestion.AnswerB = "Garden";
    bufferQuestion.AnswerC = "Kitchen";
    bufferQuestion.AnswerD = "Greenhouse";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "Why does a butcher bird kill ?";
    bufferQuestion.AnswerA = "For fun";
    bufferQuestion.AnswerB = "To eat";
    bufferQuestion.AnswerC = "To eat, only when there isn't much vegetation";
    bufferQuestion.AnswerD = "To mark it's territory";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "Who did the butcher bird be compared to ? ";
    bufferQuestion.AnswerA = "Harry";
    bufferQuestion.AnswerB = "Mr. Garfield";
    bufferQuestion.AnswerC = "Alfred";
    bufferQuestion.AnswerD = "The boy";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "Which theme best suits the ''Butcher Bird' ?";
    bufferQuestion.AnswerA = "When faced with our own faults, we shy away from bettering \nthem and instead lay our anger on the innocent";
    bufferQuestion.AnswerB = "When faced with opposition, we tend not to realize the true \ninnocence in others";
    bufferQuestion.AnswerC = "When faced with opposition, we tend not to realize the true \nvalue of others and hence lay our angers on them";
    bufferQuestion.AnswerD = "Often in our pursuit for innocence, we forget the true state \nof ourselves and recklessly lay our anger out on the innocent";
    Question.push_back(bufferQuestion);

    /** SECRET LIFE 10 **/
    bufferQuestion.Question = "What is Walter doing at the beginning of the story?";
    bufferQuestion.AnswerA = "Driving";
    bufferQuestion.AnswerB = "Shopping";
    bufferQuestion.AnswerC = "Eating beside his nagging wife at the dinner table";
    bufferQuestion.AnswerD = "Getting a checkup at the doctor";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "What was Walter day-dreaming about at the start of the story?";
    bufferQuestion.AnswerA = "He was commander of a Navy hydroplane";
    bufferQuestion.AnswerB = "He was the captain of a battleship";
    bufferQuestion.AnswerC = "He was a pilot in a fighter jet flying over the atlantic";
    bufferQuestion.AnswerD = "He was driving a tank, firing at Germans";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "Why does Mrs.Mitty interupt Walter's daydream?";
    bufferQuestion.AnswerA = "He was going over 40";
    bufferQuestion.AnswerB = "He was about to crash into something";
    bufferQuestion.AnswerC = "He was about to crash into someone";
    bufferQuestion.AnswerD = "He ran a red light";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "What was Walter's second day-dream about?";
    bufferQuestion.AnswerA = "He was a surgeon";
    bufferQuestion.AnswerB = "He was a commander of a navy battleship";
    bufferQuestion.AnswerC = "He was a millionaire banker at a house-party";
    bufferQuestion.AnswerD = "He was a broadway star at a house-party";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "Where was Walter and his wife going? ";
    bufferQuestion.AnswerA = "To town";
    bufferQuestion.AnswerB = "To a mall";
    bufferQuestion.AnswerC = "To a barbershop";
    bufferQuestion.AnswerD = "To A car mechanic";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "What were the two items Mrs. Mitty had asked Walter to buy?";
    bufferQuestion.AnswerA = "Overshoes and puppy biscuits";
    bufferQuestion.AnswerB = "Gloves and overshoes";
    bufferQuestion.AnswerC = "Apples and cooking mix";
    bufferQuestion.AnswerD = "Bedsheet and a sweater";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "What was Walter's third dream about?";
    bufferQuestion.AnswerA = "He was a witness in an epic court case";
    bufferQuestion.AnswerB = "He was a sexy seductive secret-agent";
    bufferQuestion.AnswerC = "He was a sexy millionaire playboy";
    bufferQuestion.AnswerD = "He was the president deciding how to deal with an enemy attack";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "While Walter is shopping, where is Mrs. Mitty?";
    bufferQuestion.AnswerA = "At the barber shop";
    bufferQuestion.AnswerB = "At the tanning salon";
    bufferQuestion.AnswerC = "At the doctor";
    bufferQuestion.AnswerD = "With her husband";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "What was Walter's fourth dream about?";
    bufferQuestion.AnswerA = "He was an army captain defending on the ground against German airplanes";
    bufferQuestion.AnswerB = "He didn't have a fourth dream";
    bufferQuestion.AnswerC = "He was a brave knight on the field of battle for the princess";
    bufferQuestion.AnswerD = "He was a grand chessmaster who had just delivered the winning 'checkmate' in the world chess finals ";
    Question.push_back(bufferQuestion);

    bufferQuestion.Question = "What was Walter's last dream about?";
    bufferQuestion.AnswerA = "He was facing a firing squad ready to execute him, while smoking his last cigarette";
    bufferQuestion.AnswerB = "He was a cowboy, wounded in a shootout";
    bufferQuestion.AnswerC = "A space commander, underfire from the aliens";
    bufferQuestion.AnswerD = "A secret agent, captured at the evil overlords mansion";
    Question.push_back(bufferQuestion);
}

cQMultipleChoice * cTest::getRandomQuestion(const int subject) {
    int random = 0;
    if(subject == SUBJECT::MACBETH) {
        random = randInt(0,14);
    }
    if(subject == SUBJECT::LOTF) {
        random = randInt(15,29);
    }
    if(subject == SUBJECT::DOLLS_HOUSE) {
        random = randInt(30,38);
    }
    if(subject == SUBJECT::TERMS) {
        random = randInt(39,53);
    }
    if(subject == SUBJECT::THE_FLY) {
        random = randInt(54,63);
    }
    if(subject == SUBJECT::THE_GIRAFFE) {
        random = randInt(64,73);
    }
    if(subject == SUBJECT::AFTER_YOU) {
        random = randInt(74,83);
    }
    if(subject == SUBJECT::BROTHER_DEATH) {
        random = randInt(84,93);
    }
    if(subject == SUBJECT::BUTCHER_BIRD) {
        random = randInt(94,103);
    }
    if(subject == SUBJECT::SECRET_LIFE) {
        random = randInt(104,113);
    }
    return &Question[random];
}

int randInt(int min,int max) {
    return rand()%(max-min)+min;
}
