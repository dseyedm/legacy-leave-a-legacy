#ifndef STATE_TEST_H
#define STATE_TEST_H
#include "../../../globals/globals.h"
#include "../states.h"
#include "../../cState/cState.h"
#include "cTest/cTest.h"
namespace STATE {
/**
    state TEST is the actual test.
**/
class TEST : public cState {
private:
    TEST() : cState("TEST") {};
    static TEST* instance;

    cTest Test;
    sf::Clock Timer;
    int CurrentSubject;

    int CurrentQuestion;
    int TotalQuestions;

    int QuestionsRight;

    int ChosenAnswer;
    int CorrectAnswer;

    sf::Text sQuestion;
    sf::Text sAnswerA;
    sf::Text sAnswerB;
    sf::Text sAnswerC;
    sf::Text sAnswerD;
    sf::Text isCorrect;

    sf::Texture tBackground;
    sf::Sprite  sBackground;
    sf::Texture tMenuButton;
    sf::Texture tMenuBar;
    sf::Font    fGentium;

    void NewQuestion();

    static void ChooseA();
    static void ChooseB();
    static void ChooseC();
    static void ChooseD();

    static void GotoMainMenu();
    static void NextQuestion();
    static void PlayOnHover();

    bool bMacbeth,
         bLotf,
         bDolls,
         bTerms,
         bFly,
         bGiraffe,
         bAfterYou,
         bBrotherDeath,
         bButcherBird,
         bSecretLife;
public:
    static TEST* INSTANCE();

    /** this sets a shitton of boolean variables, such as
        - Macbeth,LOTF,dollshouse,terms,thefly,giraffe,afteryou,brotherdeath,butcherbird,secretlife
        which deal with what questions are displayed
    **/
    void setTestParameters(const bool macbeth,
                           const bool lotf,
                           const bool dolls,
                           const bool terms,
                           const bool fly,
                           const bool giraffe,
                           const bool afteryou,
                           const bool brotherdeath,
                           const bool butcherbird,
                           const bool secretlife) {
        bMacbeth = macbeth;
        bLotf = lotf;
        bDolls = dolls;
        bTerms = terms;
        bFly = fly;
        bGiraffe = giraffe;
        bAfterYou = afteryou;
        bBrotherDeath = brotherdeath;
        bButcherBird = butcherbird;
        bSecretLife = secretlife;
    }
    void update();
    void render();

    void create();
    void destroy();
};
};
#endif // STATE_TEST_H
