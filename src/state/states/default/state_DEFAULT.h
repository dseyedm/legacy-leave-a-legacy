#ifndef STATE_DEFAULT_H
#define STATE_DEFAULT_H
#include "../../../globals/globals.h"
#include "../states.h"
#include "../../cState/cState.h"
namespace STATE {
/**
    state DEFAULT is only for cStateManager - the state manager's current state
    must be something upon initialization.
**/
class DEFAULT : public cState {
private:
    DEFAULT() : cState("DEFAULT") {};
    static DEFAULT* instance;
public:
    static DEFAULT* INSTANCE();
    void destroy();
};
};
#endif // STATE_DEFAULT_H
