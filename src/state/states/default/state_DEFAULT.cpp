#include "state_DEFAULT.h"
// DEFAULT
STATE::DEFAULT *STATE::DEFAULT::instance = NULL;
STATE::DEFAULT *STATE::DEFAULT::INSTANCE() {
    if(instance == NULL) {
        instance = new DEFAULT;
    }
    return instance;
}

void STATE::DEFAULT::destroy() {
    delete instance;
    instance = NULL;
}
