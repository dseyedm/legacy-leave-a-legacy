#include "cGUIBar.h"
cGUIBar::cGUIBar(const string name) : cGUIElement(name) {
    Normal = sf::Color(255,255,255);
    Shape.setFillColor(Normal);
    Shape.setSize(sf::Vector2f(250,80));
    Shape.setPosition(Shape.getPosition().x+Shape.getOutlineThickness(),
                      Shape.getPosition().y+Shape.getOutlineThickness());
    Text.setColor(sf::Color(255,255,255));
    Text.setString(Name);

    // center text on the bar
    sf::FloatRect textLocalBounds = Text.getLocalBounds();
    Text.setOrigin((int)textLocalBounds.width/2,(int)textLocalBounds.height/2);
    sf::FloatRect shapeLocalBounds = Shape.getLocalBounds();
    Text.setPosition(Shape.getPosition().x+(int)shapeLocalBounds.width/2,
                     Shape.getPosition().y+(int)shapeLocalBounds.height/2-Text.getCharacterSize()/2);
}
void cGUIBar::update() {
    //
}
void cGUIBar::render() {
    Window.draw(Shape);
    Window.draw(Text);
}
void cGUIBar::setString(const string text) {
    Text.setString(text);
    // center text on the button
    sf::FloatRect textLocalBounds = Text.getLocalBounds();
    Text.setOrigin((int)textLocalBounds.width/2,(int)textLocalBounds.height/2);
    sf::FloatRect shapeLocalBounds = Shape.getLocalBounds();
    Text.setPosition(Shape.getPosition().x+(int)shapeLocalBounds.width/2,
                     Shape.getPosition().y+(int)shapeLocalBounds.height/2-Text.getCharacterSize()/2);
}
void cGUIBar::setPosition(const sf::Vector2f pos) {
    Shape.setPosition((int)pos.x,(int)pos.y);
    // center text on the button
    sf::FloatRect textLocalBounds = Text.getLocalBounds();
    Text.setOrigin((int)textLocalBounds.width/2,(int)textLocalBounds.height/2);
    sf::FloatRect shapeLocalBounds = Shape.getLocalBounds();
    Text.setPosition(Shape.getPosition().x+(int)shapeLocalBounds.width/2,
                     Shape.getPosition().y+(int)shapeLocalBounds.height/2-Text.getCharacterSize()/2);
}
void cGUIBar::setSize(const sf::Vector2f size) {
    Shape.setSize(size);
    // center text on the button
    sf::FloatRect textLocalBounds = Text.getLocalBounds();
    Text.setOrigin((int)textLocalBounds.width/2,(int)textLocalBounds.height/2);
    sf::FloatRect shapeLocalBounds = Shape.getLocalBounds();
    Text.setPosition(Shape.getPosition().x+(int)shapeLocalBounds.width/2,
                     Shape.getPosition().y+(int)shapeLocalBounds.height/2-Text.getCharacterSize()/2);
}
