#include "cGUIButton.h"
cGUIButton::cGUIButton(const string name) : cGUIElement(name) {
    OnClickCallBack = NULL;
    OnMouseInCallBack = NULL;
    OnMouseOutCallBack = NULL;

    wasMBPressed = false;
    wasInBounds = false;
    startedInBounds = false;

    Normal = sf::Color(0,0,0);
    Tinted = sf::Color(40,40,40);

    Shape.setFillColor(Normal);
    Shape.setSize(sf::Vector2f(250,80));
    Shape.setPosition(Shape.getPosition().x+Shape.getOutlineThickness(),
                      Shape.getPosition().y+Shape.getOutlineThickness());
    Text.setColor(sf::Color(255,255,255));
    Text.setString(Name);

    // center text on the button
    sf::FloatRect textLocalBounds = Text.getLocalBounds();
    Text.setOrigin((int)textLocalBounds.width/2,(int)textLocalBounds.height/2);
    sf::FloatRect shapeLocalBounds = Shape.getLocalBounds();
    Text.setPosition(Shape.getPosition().x+(int)shapeLocalBounds.width/2,
                     Shape.getPosition().y+(int)shapeLocalBounds.height/2-Text.getCharacterSize()/2);
}
void cGUIButton::update() {
    sf::Vector2f mousePos   = Window.convertCoords(sf::Mouse::getPosition(Window));
    sf::Vector2f shapePos   = Shape.getPosition();
    sf::FloatRect shapeRect = Shape.getLocalBounds();

    const bool isMBPressed = sf::Mouse::isButtonPressed(sf::Mouse::Button::Left);
    const bool inBounds = ((mousePos.x < shapePos.x+shapeRect.width)&&
                           (mousePos.x > shapePos.x)&&
                           (mousePos.y < shapePos.y+shapeRect.height) &&
                           (mousePos.y > shapePos.y));

    // Tint, mouseIn/Out logic
    if(inBounds && !wasInBounds) {
        Shape.setFillColor(Tinted);
        if(OnMouseInCallBack != NULL)OnMouseInCallBack();
    } else if(!inBounds && wasInBounds) {
        Shape.setFillColor(Normal);
        if(OnMouseOutCallBack != NULL)OnMouseOutCallBack();
    }

    // if the mouse is held down for the first time
    if(isMBPressed && !wasMBPressed) {
        startedInBounds = inBounds;
    } else if(!isMBPressed && wasMBPressed) {
        if(startedInBounds && inBounds) {
            if(OnClickCallBack != NULL)OnClickCallBack();
        }
    }

    wasMBPressed = isMBPressed;
    wasInBounds  = inBounds;
}
void cGUIButton::render() {
    Window.draw(Shape);
    Window.draw(Text);
}
void cGUIButton::setString(const string text) {
    Text.setString(text);
    // center text on the button
    sf::FloatRect textLocalBounds = Text.getLocalBounds();
    Text.setOrigin((int)textLocalBounds.width/2,(int)textLocalBounds.height/2);
    sf::FloatRect shapeLocalBounds = Shape.getLocalBounds();
    Text.setPosition(Shape.getPosition().x+(int)shapeLocalBounds.width/2,
                     Shape.getPosition().y+(int)shapeLocalBounds.height/2-Text.getCharacterSize()/2);
}
void cGUIButton::setPosition(const sf::Vector2f pos) {
    Shape.setPosition((int)pos.x,(int)pos.y);
    // center text on the button
    sf::FloatRect textLocalBounds = Text.getLocalBounds();
    Text.setOrigin((int)textLocalBounds.width/2,(int)textLocalBounds.height/2);
    sf::FloatRect shapeLocalBounds = Shape.getLocalBounds();
    Text.setPosition(Shape.getPosition().x+(int)shapeLocalBounds.width/2,
                     Shape.getPosition().y+(int)shapeLocalBounds.height/2-Text.getCharacterSize()/2);
}
void cGUIButton::setSize(const sf::Vector2f size) {
    Shape.setSize(size);
    // center text on the button
    sf::FloatRect textLocalBounds = Text.getLocalBounds();
    Text.setOrigin((int)textLocalBounds.width/2,(int)textLocalBounds.height/2);
    sf::FloatRect shapeLocalBounds = Shape.getLocalBounds();
    Text.setPosition(Shape.getPosition().x+(int)shapeLocalBounds.width/2,
                     Shape.getPosition().y+(int)shapeLocalBounds.height/2-Text.getCharacterSize()/2);
}
