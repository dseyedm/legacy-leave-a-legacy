/**
    cGUIElement is a base class for all gui elements.
**/
#ifndef CGUIELEMENT_H
#define CGUIELEMENT_H
#include "../../globals/globals.h"
class cGUIElement {
public:
    cGUIElement(const string name) : Name(name) {};
    ~cGUIElement() {};
    const string Name;

    virtual void update() {};
    virtual void render() {};
};
#endif // CGUIELEMENT_H
