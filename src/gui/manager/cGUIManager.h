/**
    cGUIManager is a class which essentially manages all active gui elements.
    This could be buttons, text boxes, etc. buttons, text boxes, check boxes
    all derive from cGUIElement.
**/
#ifndef CGUIMANAGER_H
#define CGUIMANAGER_H
#include "../../globals/globals.h"
#include "../cGUIElement/cGUIElement.h"
class cGUIElement;
class cGUIManager {
private:
    static cGUIManager *instance;
    cGUIManager() {};
    ~cGUIManager();

    vector<cGUIElement *> Element;
public:
    static cGUIManager *getInstance();
    static void deleteInstance() {
        delete instance;
        instance = NULL;
    }

    cGUIElement *getElement(const string);

    void createElement(cGUIElement *);
    void destroyElement(const string);
    void destroyAllElements();

    void updateElements();
    void renderElements();
};
#endif // CGUIMANAGER_H
