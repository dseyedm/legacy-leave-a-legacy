#include "cGUIManager.h"
cGUIManager::~cGUIManager() {
    for(unsigned int x=0; x<Element.size(); x++) {
        delete Element[x];
    }
    Element.clear();
}

cGUIManager *cGUIManager::instance = NULL;
cGUIManager *cGUIManager::getInstance() {
    if(instance == NULL) {
        instance = new cGUIManager;
    }
    return instance;
}

cGUIElement *cGUIManager::getElement(const string name) {
    // search through all elements to match the name
    for(unsigned int x=0; x<Element.size(); x++) {
        if(name == Element[x]->Name) {
            return Element[x];
        }
    }
    // error ! No element found with that name
    cLogManager::getInstance()->log(ERR::GUIM::GET_ELEMENT_INVALID_NAME,LOG::DIR,LOG::FILE);
    cErrorManager::getInstance()->displayError(ERR::GUIM::GET_ELEMENT_INVALID_NAME,LOG::DIR,LOG::FILE);
    Window.close();

    return NULL;
}
void cGUIManager::createElement(cGUIElement *element) {
    Element.push_back(element);
}
void cGUIManager::destroyElement(const string name) {
    // search through all elements to match the name
    for(unsigned int x=0; x<Element.size(); x++) {
        if(name == Element[x]->Name) {
            delete Element[x];
            if(Element.size()>1) {
                Element[x] = Element[Element.size()-1];
            }
            Element.pop_back();
            return;
        }
    }
    // error ! No element found with that name
    cLogManager::getInstance()->log(ERR::GUIM::DESTROY_ELEMENT_INVALID_NAME,LOG::DIR,LOG::FILE);
    cErrorManager::getInstance()->displayError(ERR::GUIM::DESTROY_ELEMENT_INVALID_NAME,LOG::DIR,LOG::FILE);
    Window.close();
}
void cGUIManager::destroyAllElements() {
    for(unsigned int x=0; x<Element.size(); x++) {
        delete Element[x];
    }
    Element.clear();
}
void cGUIManager::updateElements() {
    for(unsigned int x=0; x<Element.size(); x++) {
        Element[x]->update();
    }
}
void cGUIManager::renderElements() {
    for(unsigned int x=0; x<Element.size(); x++) {
        Element[x]->render();
    }
}
