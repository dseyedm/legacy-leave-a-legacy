/**
    cGUICheckbox is a gui element. It contains a rectangle shape, text BESIDE the button,
    and various functionality like OnMouseOut or OnMouseIn as well as OnClick.
**/
#ifndef CGUICHECKBOX_H
#define CGUICHECKBOX_H
#include "../globals/globals.h"
#include "cGUIElement/cGUIElement.h"
typedef void (*VoidFnCallBack)();
class cGUICheckbox : public cGUIElement {
private:
    VoidFnCallBack OnClickCallBack;
    VoidFnCallBack OnMouseInCallBack;
    VoidFnCallBack OnMouseOutCallBack;

    const float TextOffset;

    bool wasMBPressed;
    bool wasInBounds;
    bool startedInBounds;
public:
    cGUICheckbox(const string);
    ~cGUICheckbox() {};

    bool isSelected;

    sf::Color Normal;
    sf::Color Tinted;

    sf::RectangleShape Shape;
    sf::Text Text;

    void setString(const string);
    void setPosition(const sf::Vector2f);

    void update();
    void render();

    void setClickCallBack    (VoidFnCallBack onClickCB) {
        OnClickCallBack    = onClickCB;
    };
    void setMouseInCallBack  (VoidFnCallBack onMouseInCB) {
        OnMouseInCallBack  = onMouseInCB;
    };
    void setMouseOutCallBack (VoidFnCallBack onMouseOutCB) {
        OnMouseOutCallBack = onMouseOutCB;
    };
};
#endif // CGUICHECKBOX_H
