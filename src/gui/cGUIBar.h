/**
    cGUIBar is a gui element. It contains a rectangle shape, text on top of the button,
    and thats it. A simple bar with a string on top.
**/
#ifndef CGUI_BAR_H
#define CGUI_BAR_H
#include "../globals/globals.h"
#include "cGUIElement/cGUIElement.h"
class cGUIBar : public cGUIElement {
private:
public:
    cGUIBar(const string);
    ~cGUIBar() {};

    sf::RectangleShape Shape;
    sf::Text Text;

    sf::Color Normal;

    void setString(const string);
    void setPosition(const sf::Vector2f);
    void setSize(const sf::Vector2f);

    void update();
    void render();
};
#endif // CGUI_BAR_H
