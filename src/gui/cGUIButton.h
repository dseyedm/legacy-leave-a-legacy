/**
    cGUIButton is a gui element. It contains a rectangle shape, text on top of the button,
    and various functionality like OnMouseOut or OnMouseIn as well as OnClick.
**/
#ifndef CGUIBUTTON_H
#define CGUIBUTTON_H
#include "../globals/globals.h"
#include "cGUIElement/cGUIElement.h"
typedef void (*VoidFnCallBack)();
class cGUIButton : public cGUIElement {
private:
    VoidFnCallBack OnClickCallBack;
    VoidFnCallBack OnMouseInCallBack;
    VoidFnCallBack OnMouseOutCallBack;

    bool wasMBPressed;
    bool wasInBounds;
    bool startedInBounds;
public:
    cGUIButton(const string);
    ~cGUIButton() {};

    sf::Color Normal;
    sf::Color Tinted;

    sf::RectangleShape Shape;
    sf::Text Text;

    void setString(const string);
    void setPosition(const sf::Vector2f);
    void setSize(const sf::Vector2f);

    void update();
    void render();

    void setClickCallBack    (VoidFnCallBack onClickCB) {
        OnClickCallBack    = onClickCB;
    };
    void setMouseInCallBack  (VoidFnCallBack onMouseInCB) {
        OnMouseInCallBack  = onMouseInCB;
    };
    void setMouseOutCallBack (VoidFnCallBack onMouseOutCB) {
        OnMouseOutCallBack = onMouseOutCB;
    };
};
#endif // CGUIBUTTON_H
