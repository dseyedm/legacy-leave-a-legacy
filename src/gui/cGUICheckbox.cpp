#include "cGUICheckbox.h"
cGUICheckbox::cGUICheckbox(const string name) : cGUIElement(name), TextOffset(50) {
    OnClickCallBack = NULL;
    OnMouseInCallBack = NULL;
    OnMouseOutCallBack = NULL;

    wasMBPressed = false;
    wasInBounds = false;
    startedInBounds = false;

    isSelected = false;

    Normal = sf::Color(0,0,0);
    Tinted = sf::Color(255,255,255);

    Shape.setFillColor(Normal);
    Shape.setSize(sf::Vector2f(42,42));
    Shape.setPosition(Shape.getPosition().x+Shape.getOutlineThickness(),
                      Shape.getPosition().y+Shape.getOutlineThickness());
    Text.setColor(sf::Color(255,255,255));
    Text.setString(Name);

    Text.setOrigin(0,Text.getLocalBounds().height/2);
    // text beside the button
    Text.setPosition((int)Shape.getPosition().x+(int)TextOffset,
                     (int)Shape.getPosition().y);
    Text.setOrigin(0,0);
}
void cGUICheckbox::update() {
    sf::Vector2f mousePos   = Window.convertCoords(sf::Mouse::getPosition(Window));
    sf::Vector2f shapePos   = Shape.getPosition();
    sf::FloatRect shapeRect = Shape.getLocalBounds();

    const bool isMBPressed = sf::Mouse::isButtonPressed(sf::Mouse::Button::Left);
    const bool inBounds = ((mousePos.x < shapePos.x+shapeRect.width)&&
                           (mousePos.x > shapePos.x)&&
                           (mousePos.y < shapePos.y+shapeRect.height) &&
                           (mousePos.y > shapePos.y));

    // Tint, mouseIn/Out logic
    if(inBounds && !wasInBounds) {
        Shape.setFillColor(Tinted);
        if(OnMouseInCallBack != NULL)OnMouseInCallBack();
    } else if(!inBounds && wasInBounds) {
        Shape.setFillColor(Normal);
        if(OnMouseOutCallBack != NULL)OnMouseOutCallBack();
    }

    // if the mouse is held down for the first time
    if(isMBPressed && !wasMBPressed) {
        startedInBounds = inBounds;
    } else if(!isMBPressed && wasMBPressed) {
        if(startedInBounds && inBounds) {
            isSelected = !isSelected;
            if(OnClickCallBack != NULL)OnClickCallBack();
        }
    }

    wasMBPressed = isMBPressed;
    wasInBounds  = inBounds;
}
void cGUICheckbox::render() {
    Window.draw(Shape);
    Window.draw(Text);
}
void cGUICheckbox::setString(const string text) {
    Text.setOrigin(0,Text.getLocalBounds().height/2);
    Text.setString(text);
    // text beside the button
    Text.setPosition((int)Shape.getPosition().x+(int)TextOffset,
                     (int)Shape.getPosition().y);
    Text.setOrigin(0,0);
}
void cGUICheckbox::setPosition(const sf::Vector2f pos) {
    Text.setOrigin(0,Text.getLocalBounds().height/2);
    Shape.setPosition((int)pos.x,(int)pos.y);
    // text beside the button
    Text.setPosition((int)Shape.getPosition().x+(int)TextOffset,
                     (int)Shape.getPosition().y);
    Text.setOrigin(0,0);
}
