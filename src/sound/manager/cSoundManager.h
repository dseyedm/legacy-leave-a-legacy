/**
    The sound manager plays and stores sound.
**/
#ifndef CSOUNDMANAGER_H
#define CSOUNDMANAGER_H
#include "../../globals/globals.h"
class cSoundManager {
private:
    cSoundManager() {};
    ~cSoundManager() {};
    static cSoundManager *instance;

    vector<string *> SoundFile;   // Holds the file paths that have already been loaded
    vector<sf::SoundBuffer *> SoundBuffer;
public:
    static cSoundManager *getInstance();
    static void deleteInstance();

    sf::Sound Player;

    void load(const string);
    void play(const string);
};

#endif // CSOUNDMANAGER_H
