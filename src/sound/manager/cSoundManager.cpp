#include "cSoundManager.h"
cSoundManager *cSoundManager::instance = NULL;
cSoundManager *cSoundManager::getInstance() {
    if(instance == NULL) {
        instance = new cSoundManager;
    }
    return instance;
}
void cSoundManager::deleteInstance() {
    for(unsigned int x=0; x<instance->SoundFile.size(); x++) {
        delete instance->SoundFile[x];
    }
    for(unsigned int x=0; x<instance->SoundBuffer.size(); x++) {
        delete instance->SoundBuffer[x];
    }
    delete instance;
    instance = NULL;
}
void cSoundManager::load(const string file) {
    bool matchFound = false;
    // check if file already exists
    for(unsigned int x=0; x<SoundFile.size(); x++) {
        if(file == *SoundFile[x]) {
            // match found !
            matchFound = true;
            break;
        }
    }
    if(!matchFound) {
        // assign a new file
        SoundFile.push_back(new string);
        *SoundFile[SoundFile.size()-1] = file;

        SoundBuffer.push_back(new sf::SoundBuffer);
        // if not, attempt load the file and file
        if(!SoundBuffer[SoundBuffer.size()-1]->loadFromFile(file)) {
            cLogManager::getInstance()->log("could not load '" + file + "'\n",LOG::DIR,LOG::FILE);
            cErrorManager::getInstance()->displayError("could not load '" + file + "'\n",LOG::DIR,LOG::FILE);
        }
    }
}
void cSoundManager::play(const string file) {
    // if file matches
    bool matchFound = false;
    unsigned int matchedElement = 0;
    for(unsigned int x=0; x<SoundFile.size(); x++) {
        if(file == *SoundFile[x]) {
            matchFound = true;
            matchedElement = x;
            break;
        }
    }
    if(matchFound) {
        // set sound buffer from string
        Player.stop();
        Player.setBuffer(*SoundBuffer[matchedElement]);
        Player.play();
    } else {
        cLogManager::getInstance()->log("could not play '" + file + "'\n",LOG::DIR,LOG::FILE);
        cErrorManager::getInstance()->displayError("could not play '" + file + "'\n",LOG::DIR,LOG::FILE);
    }
}
