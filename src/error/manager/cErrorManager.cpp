#include "cErrorManager.h"
cErrorManager *cErrorManager::instance = NULL;
cErrorManager *cErrorManager::getInstance() {
    if(instance == NULL) {
        instance = new cErrorManager;
        instance->Error = false;
    }
    return instance;
}

void cErrorManager::displayError(const string error,const string log_dir,const string log_file) {
    Error = true;
    string buffer = "error: " + error + "logged in " + log_dir + log_file;
    MessageBox(NULL,buffer.c_str(),"Fatal error",MB_OK|MB_ICONERROR);
}
