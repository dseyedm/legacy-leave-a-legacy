/**
    cErrorManager is a class which handles the displaying of errors.
    It uses win32 to display a simple message box to the user in
    case anything goes wrong.
**/
#ifndef CERRORMANAGER_H
#define CERRORMANAGER_H
#include "../../globals/globals.h"
class cErrorManager {
private:
    cErrorManager() {};
    ~cErrorManager() {};
    static cErrorManager *instance;
    bool Error;
public:
    static cErrorManager *getInstance();
    static void deleteInstance() {
        delete instance;
        instance = NULL;
    }

    void displayError(const string,const string,const string);
    bool error() {
        return Error;
    };
};

#endif // CERRORMANAGER_H
