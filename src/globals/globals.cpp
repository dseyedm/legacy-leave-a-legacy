#include "globals.h"
sf::RenderWindow Window;
bool directoryExists(const string dir) {
    DWORD fileAttributes = GetFileAttributesA(dir.c_str());
    if (fileAttributes == INVALID_FILE_ATTRIBUTES)
        return false;

    if (fileAttributes & FILE_ATTRIBUTE_DIRECTORY)
        return true;

    return false;
}
