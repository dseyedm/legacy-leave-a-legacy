/**
    globals.h is for global includes, variables and objects
    Every header file includes this file
**/
#ifndef GLOBALS_H
#define GLOBALS_H
/**
    define all includes
**/
#ifdef G_DEBUG
#include <iostream>
#endif
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <SFML/Window.hpp>
#include <SFML/System.hpp>
#include <sstream>
#include <cstdlib>
#include <time.h>
using namespace std;

/**
    objects and variables
**/
extern sf::RenderWindow Window;

/**
    functions
**/
bool directoryExists(const string);

/**
    singletons
**/
#include "../state/manager/cStateManager.h" // include the singleton of cStateManager
#include "../logger/manager/cLogManager.h"  // include the singleton of cLogManager
#include "../error/manager/cErrorManager.h" // include the singleton of cErrorManager
#include "../gui/manager/cGUIManager.h"     // include the singleton of cGUIManager
#include "../sound/manager/cSoundManager.h" // include the singleton of cSoundManager

/**
    constants, namespaces and defines
**/
// log namespace, which deals with where things are logged
namespace LOG {
const string DIR  = "data/log/";
const string FILE = "log.txt";
};
// file namespace, which deals with loading files
namespace FILEPATH {
namespace TEXTURE {
const string BACKGROUND_TESTOPTION = "data/texture/background_testoption.png";
const string BACKGROUND_TEST = "data/texture/background_test.png";
const string BACKGROUND_TESTEND = "data/texture/background_testend.png";
const string BACKGROUND_MAINMENU = "data/texture/background_mainmenu.png";
const string BACKGROUND_CREDITS = "data/texture/background_credits.png";
const string CREDITS_OVERLAY = "data/texture/credits_overlay.png";
const string MENUBUTTON = "data/texture/menubutton.png";
const string MENUBAR = "data/texture/menubar.png";
const string CHECKBOXBLANK   = "data/texture/checkboxblank.png";
const string CHECKBOXMARKED   = "data/texture/checkboxmarked.png";
};
namespace SOUND {
const string ALERT_1  = "data/sound/alert_1.wav";
const string ALERT_2  = "data/sound/alert_2.wav";
const string CLICK_1  = "data/sound/click_1.wav";
const string CLICK_2  = "data/sound/click_2.wav";
const string CLICK_3  = "data/sound/click_3.wav";
const string CLICK_4  = "data/sound/click_4.wav";
const string ERROR_1  = "data/sound/error_1.wav";
const string POP      = "data/sound/pop.wav";
const string ROLLOVER = "data/sound/rollover.wav";
const string SHIF     = "data/sound/shif.wav";
const string SWIP     = "data/sound/swip.wav";
};
namespace FONT {
const string GENTIUM = "data/font/gentium.ttf";
};
};

// error namespace, which deals with strings displayed to the user when something fails
namespace ERR {
// general file load failure errors
namespace NOLOAD {
namespace TEXTURE {
const string BACKGROUND_TESTOPTION = "could not load '" + FILEPATH::TEXTURE::BACKGROUND_TESTOPTION + "'\n";
const string BACKGROUND_TEST = "could not load '" + FILEPATH::TEXTURE::BACKGROUND_TEST + "'\n";
const string BACKGROUND_MAINMENU = "could not load '" + FILEPATH::TEXTURE::BACKGROUND_MAINMENU + "'\n";
const string BACKGROUND_CREDITS = "could not load '" + FILEPATH::TEXTURE::BACKGROUND_CREDITS + "'\n";
const string BACKGROUND_TESTEND = "could not load '" + FILEPATH::TEXTURE::BACKGROUND_TESTEND + "'\n";
const string CREDITS_OVERLAY = "could not load '" + FILEPATH::TEXTURE::CREDITS_OVERLAY + "'\n";
const string MENUBUTTON = "could not load '" + FILEPATH::TEXTURE::MENUBUTTON + "'\n";
const string MENUBAR = "could not load '" + FILEPATH::TEXTURE::MENUBAR + "'\n";
const string CHECKBOXBLANK  = "could not load '" + FILEPATH::TEXTURE::CHECKBOXBLANK  + "'\n";
const string CHECKBOXMARKED = "could not load '" + FILEPATH::TEXTURE::CHECKBOXMARKED + "'\n";
};
namespace SOUND {
const string ALERT_1  = "could not load '" + FILEPATH::SOUND::ALERT_1  + "'\n";
const string ALERT_2  = "could not load '" + FILEPATH::SOUND::ALERT_2  + "'\n";
const string CLICK_1  = "could not load '" + FILEPATH::SOUND::CLICK_1  + "'\n";
const string CLICK_2  = "could not load '" + FILEPATH::SOUND::CLICK_2  + "'\n";
const string CLICK_3  = "could not load '" + FILEPATH::SOUND::CLICK_3  + "'\n";
const string CLICK_4  = "could not load '" + FILEPATH::SOUND::CLICK_4  + "'\n";
const string ERROR_1  = "could not load '" + FILEPATH::SOUND::ERROR_1  + "'\n";
const string POP      = "could not load '" + FILEPATH::SOUND::POP      + "'\n";
const string ROLLOVER = "could not load '" + FILEPATH::SOUND::ROLLOVER + "'\n";
const string SHIF     = "could not load '" + FILEPATH::SOUND::SHIF     + "'\n";
const string SWIP     = "could not load '" + FILEPATH::SOUND::SWIP     + "'\n";
};
namespace FONT {
const string GENTIUM = "could not load '" + FILEPATH::FONT::GENTIUM + "'\n";
};
};
// errors specific to managers and states
namespace GUIM {
const string GET_ELEMENT_INVALID_NAME     = "getElement([invalid name]) called\n";
const string DESTROY_ELEMENT_INVALID_NAME = "destroyElement([invalid name]) called\n";
};
namespace LOGM {
const string SET_STATE_NULL = "setState(NULL) called\n";
const string GET_STATE_NULL = "getState() returned NULL\n";
};
};
#endif // GLOBALS_H
