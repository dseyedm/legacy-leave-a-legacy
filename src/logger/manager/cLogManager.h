/**
    The log manager does exactly what its name implies - it logs to a file.
**/
#ifndef CLOGMANAGER_H
#define CLOGMANAGER_H
#include "../../globals/globals.h"
#include <windows.h>
#include <fstream>
class cLogManager {
private:
    cLogManager() {};
    ~cLogManager() {};
    static cLogManager *instance;

    vector<string> FileDirectory;   // Holds the files that have already been logged
public:
    static cLogManager *getInstance();
    static void deleteInstance() {
        delete instance;
        instance = NULL;
    }

    void log(const string,const string,const string);
};

#endif // CLOGMANAGER_H
