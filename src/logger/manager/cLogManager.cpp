#include "cLogManager.h"
cLogManager *cLogManager::instance = NULL;
cLogManager *cLogManager::getInstance() {
    if(instance == NULL) {
        instance = new cLogManager;
    }
    return instance;
}

void cLogManager::log(const string text,const string directory,const string filename) {
    // fileDir is the directory + '/' + filename
    string fileDir = directory;
    if(fileDir[fileDir.size()-1] != '/') {
        fileDir.push_back('/');
    }
    fileDir.append(filename);

    // check if the file has already been registered
    bool hasBeenRegistered = false;
    for(unsigned int x=0; x<FileDirectory.size(); x++) {
        if(fileDir == FileDirectory[x]) {
            hasBeenRegistered = true;
        }
    }

    if(!hasBeenRegistered) {
        // register it
        FileDirectory.push_back(fileDir);

        // create the directory
        string Dir = "";
        for(unsigned int x=0; x<fileDir.size(); x++) {
            if(fileDir[x] == '/') {
                Dir.push_back(fileDir[x]);
                if(!directoryExists(Dir)) {
                    CreateDirectory(Dir.c_str(),NULL);
                }
            } else
                Dir.push_back(fileDir[x]);
        }
    }

    // now log to the file
    ofstream fileStream;
    if(!hasBeenRegistered)  fileStream.open(fileDir.c_str());           // if the file has not been registered, overwrite it
    else                    fileStream.open(fileDir.c_str(),ios::app);  // else, append to it
    fileStream << text;
    fileStream.close();

    // now cout if debug version
#ifdef G_DEBUG
    cout << text;
#endif // G_DEBUG
}
